﻿using Azfibernet.Employees.Dtos;
using Azfibernet.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Azfibernet.Employees
{    
    public class EmployeeController : BaseController
    {
        private readonly IEmployeeService _employeeService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public EmployeeController(IEmployeeService employeeService, IHttpContextAccessor httpContextAccessor)
        {
            _employeeService = employeeService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("employees")]
        //[CustomAuthorize("employee:list")]
        public async Task<IActionResult> GetAllAsync([FromQuery] GetAllEmployeesDto input)
        {
            return Ok(await _employeeService.GetAllAsync(input));
        }

        [HttpGet("departments")]
        //[CustomAuthorize("employee:add", "employee:edit", "employee:list")]
        public async Task<IActionResult> GetDepartmentsAsync()
        {
            return Ok(await _employeeService.GetDepartmentsAsync());
        }

        [HttpGet("education-degrees")]
        //[CustomAuthorize("employee:add", "employee:edit", "employee:list")]
        public async Task<IActionResult> GetEducationDegreesAsync()
        {
            return Ok(await _employeeService.GetEducationDegreesAsync());
        }

        [HttpGet("employment-types")]
        //[CustomAuthorize("employee:add", "employee:edit", "employee:list")]
        public async Task<IActionResult> GetEmploymentTypesAsync()
        {
            return Ok(await _employeeService.GetEmploymentTypesAsync());
        }

        [HttpGet("marital-statuses")]
        //[CustomAuthorize("employee:add", "employee:edit", "employee:list")]
        public async Task<IActionResult> GetMaritalStatusesAsync()
        {
            return Ok(await _employeeService.GetMaritalStatusesAsync());
        }

        [HttpGet("staff-types")]
        //[CustomAuthorize("employee:add", "employee:edit", "employee:list")]
        public async Task<IActionResult> GetStaffTypesAsync()
        {
            return Ok(await _employeeService.GetStaffTypesAsync());
        }

        [HttpGet("work-schedule-types")]
        //[CustomAuthorize("employee:add", "employee:edit", "employee:list")]
        public async Task<IActionResult> GetWorkScheduleTypes()
        {
            return Ok(await _employeeService.GetWorkScheduleTypesAsync());
        }

        [HttpGet("work-place-types")]
        //[CustomAuthorize("employee:add", "employee:edit", "employee:list")]
        public async Task<IActionResult> GetWorkPlaceTypesAsync()
        {
            return Ok(await _employeeService.GetWorkPlaceTypesAsync());
        }

        [HttpGet("genders")]
        //[CustomAuthorize("employee:add", "employee:edit", "employee:list")]
        public async Task<IActionResult> GetGendersAsync()
        {
            return Ok(await _employeeService.GetGendersAsync());
        }

        [HttpGet("contract-types")]
        //[CustomAuthorize("contract:add")]
        public async Task<IActionResult> GetContractTypesAsync()
        {
            return Ok(await _employeeService.GetContractTypesAsync());
        }

        [HttpGet("order-types")]
        //[CustomAuthorize("order:add")]
        public async Task<IActionResult> GetOrderTypesAsync()
        {
            return Ok(await _employeeService.GetOrderTypesAsync());
        }

        [HttpGet("document-types")]
        //[CustomAuthorize("document:add, document:edit")]
        public async Task<IActionResult> GetDocumentTypesAsync()
        {
            return Ok(await _employeeService.GetDocumentTypesAsync());
        }
    }
}
