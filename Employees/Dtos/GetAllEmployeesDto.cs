﻿using Azfibernet.Shared.Search.Implements;

namespace Azfibernet.Employees.Dtos
{
    public class GetAllEmployeesDto : PagedAndSortedResultRequestDto
    {
        public string FullName { get; set; }
        public int? DepartmentId { get; set; }
        public int? MinAge { get; set; }
        public int? MaxAge { get; set; }
        public int? WorkScheduleTypeId { get; set; }
        public int? EmploymentTypeId { get; set; }
        public int? StaffTypeId { get; set; }
        public bool? IsArchived { get; set; }
        public int? WorkPlaceTypeId { get; set; }
        public int? GenderId { get; set; }
    }
}
