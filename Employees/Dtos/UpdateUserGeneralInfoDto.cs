﻿namespace Azfibernet.Employees.Dtos
{
    public class UpdateUserGeneralInfoDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public int PositionId { get; set; }

    }
}
