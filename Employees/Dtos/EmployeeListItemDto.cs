﻿namespace Azfibernet.Employees.Dtos
{
    public class EmployeeListItemDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Department { get; set; }
        public int? PositionId { get; set; }
        public string Position { get; set; }
        public string EmailAddress { get; set; }
        public string CorporationPhoneNumber { get; set; }
        public bool IsArchived { get; set; }
    }
}
