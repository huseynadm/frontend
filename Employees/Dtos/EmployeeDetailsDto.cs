﻿using System;

namespace Azfibernet.Employees.Dtos
{
    public class EmployeeDetailsDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string PersonalIdentificationNumber { get; set; }
        public string IdCardSerialNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Citizenship { get; set; }
        public string Nationality { get; set; }
        public string EducationDegree { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public string CorporationPhoneNumber { get; set; }
        public string InternalPhoneNumber { get; set; }
        public string Company { get; set; }
        public string Department { get; set; }
        public string EmploymentType { get; set; }
        public string Position { get; set; }
        public string WorkScheduleType { get; set; }
        public string WeeklyWorkHours { get; set; }
        public string StaffType { get; set; }
        public string WorkPlaceType { get; set; }
        public string SocialInsuaranceCardNumber { get; set; }
    }
}
