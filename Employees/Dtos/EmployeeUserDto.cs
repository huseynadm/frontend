﻿namespace Azfibernet.Employees.Dtos
{
    public class EmployeeUserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public int? PositionId { get; set; }
    }
}
