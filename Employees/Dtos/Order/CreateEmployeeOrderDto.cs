﻿using System;
using System.Text.Json.Serialization;

namespace Azfibernet.Employees.Dtos.Order
{
    public class CreateEmployeeOrderDto
    {
        [JsonIgnore]
        public int EmployeeId { get; set; }

        public int OrderTypeId { get; set; }
        public string Number { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
