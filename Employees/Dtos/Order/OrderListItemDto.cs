﻿using System;

namespace Azfibernet.Employees.Dtos.Order
{
    public class OrderListItemDto
    {
        public string OrderType { get; set; }
        public string Number { get; set; }
        public DateTime OrderDate { get; set; }
        public string CreatorUser { get; set; }
        public int CreatorUserId { get; set; }
    }
}
