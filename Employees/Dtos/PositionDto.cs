﻿namespace Azfibernet.Employees.Dtos
{
    public class PositionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
