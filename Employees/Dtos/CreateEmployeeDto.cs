﻿using System;

namespace Azfibernet.Employees.Dtos
{
    public class CreateEmployeeDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int GenderId { get; set; }
        public int? MaritalStatusId { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PersonalIdentificationNumber { get; set; }
        public string IdCardSerialNumber { get; set; }
        public string SocialInsuaranceCardNumber { get; set; }
        public string Company { get; set; }
        public int? DepartmentId { get; set; }
        public int? PositionId { get; set; }
        public int? StaffTypeId { get; set; }
        public int? EmploymentTypeId { get; set; }
        public int? WorkPlaceTypeId { get; set; }
        public int? WorkScheduleTypeId { get; set; }
        public string WeeklyWorkHours { get; set; }
        public string Nationality { get; set; }
        public string Citizenship { get; set; }
        public int? EducationDegreeId { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public string CorporationPhoneNumber { get; set; }
        public string InternalPhoneNumber { get; set; }
    }
}
