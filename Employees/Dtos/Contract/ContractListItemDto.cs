﻿using System;

namespace Azfibernet.Employees.Dtos.Contract
{
    public class ContractListItemDto
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ContractType { get; set; }
        public string Number { get; set; }
        public string WorkbookNumber { get; set; }
        public bool IsPermanent { get; set; }
    }
}
