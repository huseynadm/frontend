﻿using System;
using System.Text.Json.Serialization;

namespace Azfibernet.Employees.Dtos.Contract
{
    public class CreateEmployeeContractDto
    {
        [JsonIgnore]
        public int EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ContractTypeId { get; set; }
        public string Number { get; set; }
        public string WorkbookNumber { get; set; }
        public bool IsPermanent { get; set; }
    }
}
