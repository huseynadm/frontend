﻿using System.Text.Json.Serialization;

namespace Azfibernet.Employees.Dtos
{
    public class UpdateEmployeeDto : CreateEmployeeDto
    {
        [JsonIgnore]
        public int Id { get; set; }
    }
}
