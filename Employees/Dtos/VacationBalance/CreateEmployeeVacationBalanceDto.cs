using System.Text.Json.Serialization;
using System;

namespace Azfibernet.Employees.Dtos.VacationBalance
{
    public class CreateEmployeeVacationBalanceDto
    {
        [JsonIgnore]
        public int EmployeeId { get; set; }

        public DateTime WorkYearStartDate { get; set; }
        public DateTime WorkYearEndDate { get; set; }
        public int MainVacationDays { get; set; }
        public int? AdditionalVacationDays { get; set; }
        public int? UsedVacationDays { get; set; }
    }
}