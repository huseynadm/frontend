﻿using Azfibernet.Employees.Models;
using System;

namespace Azfibernet.Employees.Dtos.VacationBalance
{
    public class EmployeeVacationBalanceDto
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime WorkYearStartDate { get; set; }
        public DateTime WorkYearEndDate { get; set; }
        public int MainVacationDays { get; set; }
        public int? AdditionalVacationDays { get; set; }
        public int? UsedVacationDays { get; set; }
        public int RemainingVacationDays
        {
            get
            {
                return MainVacationDays + (AdditionalVacationDays ?? 0) - (UsedVacationDays ?? 0);
            }
        }
        public Employee Employee { get; set; }
    }
}
