﻿using System;

namespace Azfibernet.Employees.Dtos.VacationBalance
{
    public class VacationBalanceListItemDto
    {
        public int Id { get; set; }
        public DateTime WorkYearStartDate { get; set; }
        public DateTime WorkYearEndDate { get; set; }
        public int MainVacationDays { get; set; }
        public int? AdditionalVacationDays { get; set; }
        public int? UsedVacationDays { get; set; }
        public int RemainingVacationDays 
        {
            get => MainVacationDays + (AdditionalVacationDays ?? 0) - (UsedVacationDays ?? 0);
        }
    }
}
