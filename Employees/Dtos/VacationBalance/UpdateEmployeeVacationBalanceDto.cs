﻿using System.Text.Json.Serialization;

namespace Azfibernet.Employees.Dtos.VacationBalance
{
    public class UpdateEmployeeVacationBalanceDto : CreateEmployeeVacationBalanceDto
    {
        [JsonIgnore]
        public int Id { get; set; }
    }
}
