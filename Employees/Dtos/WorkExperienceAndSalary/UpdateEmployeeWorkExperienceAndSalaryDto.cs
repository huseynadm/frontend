﻿using System;
using System.Text.Json.Serialization;

namespace Azfibernet.Employees.Dtos.WorkExperienceAndSalary
{
    public class UpdateEmployeeWorkExperienceAndSalaryDto
    {
        [JsonIgnore]
        public int EmployeeId { get; set; }

        public string TotalPastWorkExperience { get; set; }
        public DateTime? HireDate { get; set; }
        public decimal? GrossSalary { get; set; }
        public decimal? NetSalary { get; set; }
    }
}
