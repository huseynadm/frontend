﻿using System;
using System.Linq;

namespace Azfibernet.Employees.Dtos.WorkExperienceAndSalary
{
    public class WorkExperienceAndSalaryDto
    {
        public string TotalPastWorkExperience { get; set; }

        public string TotalPastworkExperienceDetails
        {
            get
            {
                return WorkExperienceDetails(SplitTotalPastWorkExperienceToDateParts(TotalPastWorkExperience));
            }
        }

        public string CurrentWorkExperienceDetails
        {
            get
            {
                return WorkExperienceDetails(FindDiffBetweenHireDateAndToday(HireDate));
            }
        }

        public string TotalWorkExperience
        {
            get
            {
                return CalculateTotalWorkExperience(FindDiffBetweenHireDateAndToday(HireDate),
                    SplitTotalPastWorkExperienceToDateParts(TotalPastWorkExperience));
            }
        }

        public DateTime? HireDate { get; set; }
        public decimal? GrossSalary { get; set; }
        public decimal? NetSalary { get; set; }

        private string WorkExperienceDetails(int[] dateParts)
        {
            var days = dateParts[0] > 0 ? $"{dateParts[0]}gün" : string.Empty;
            var months = dateParts[1] > 0 ? $"{dateParts[1]}ay " : string.Empty;
            var years = dateParts[2] > 0 ? $"{dateParts[2]}il " : string.Empty;

            return $"{years}{months}{days}";
        }

        private int[] FindDiffBetweenHireDateAndToday(DateTime? hireDate)
        {
            hireDate ??= DateTime.Today;

            var diffInDays = (DateTime.Today - hireDate).Value.Days / 365.25;

            var year = (int)diffInDays;
            var month = (diffInDays - year) * 12;
            var day = (int)((month - (int)month) * 30);

            return new int[] { day, (int)month, year };
        }

        private int[] SplitTotalPastWorkExperienceToDateParts(string totalPastWorkExperience)
        {
            if (string.IsNullOrEmpty(totalPastWorkExperience))
            {
                return new int[] { 0, 0, 0 };
            }
            return totalPastWorkExperience.Split("/").Select(e => Convert.ToInt32(e)).ToArray();
        }

        private string CalculateTotalWorkExperience(int[] currentWorkExperience, int[] totalPastWorkExperience)
        {
            int[] dateParts = new int[3];
            var isDaysOverflow = false;
            var isMonthesOverflow = false;

            dateParts[0] = currentWorkExperience[0] + totalPastWorkExperience[0];
            if (dateParts[0] >= 30)
            {
                dateParts[0] -= 30;
                isDaysOverflow = true;
            }

            dateParts[1] = currentWorkExperience[1] + totalPastWorkExperience[1] + (isDaysOverflow ? 1 : 0);
            if (dateParts[1] >= 12)
            {
                dateParts[1] -= 12;
                isMonthesOverflow = true;
            }

            dateParts[2] = currentWorkExperience[2] + totalPastWorkExperience[2] + (isMonthesOverflow ? 1 : 0);

            return WorkExperienceDetails(dateParts);
        }
    }
}