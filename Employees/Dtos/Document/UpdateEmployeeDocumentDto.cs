﻿using System.Text.Json.Serialization;

namespace Azfibernet.Employees.Dtos.Document
{
    public class UpdateEmployeeDocumentDto : CreateEmployeeDocumentDto
    {
        [JsonIgnore]
        public int Id { get; set; }
    }
}
