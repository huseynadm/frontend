﻿using Microsoft.AspNetCore.Http;
using System.Text.Json.Serialization;

namespace Azfibernet.Employees.Dtos.Document
{
    public class CreateEmployeeDocumentDto
    {
        [JsonIgnore]
        public int EmployeeId { get; set; }

        public int EmployeeDocumentTypeId { get; set; }
        public string Description { get; set; }
        public IFormFile Document { get; set; }
    }
}
