﻿using System.Text.Json.Serialization;

namespace Azfibernet.Employees.Dtos.Document
{
    public class EmployeeDocumentDto
    {        
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileHash { get; set; }
        public string FileContentType { get; set; }
        public long FileLength { get; set; }     

        [JsonIgnore]
        public byte[] FileContent { get; set; }
    }
}
