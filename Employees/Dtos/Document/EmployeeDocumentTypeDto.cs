﻿namespace Azfibernet.Employees.Dtos.Document
{
    public class EmployeeDocumentTypeDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string AllowedExtensions { get; set; }
    }
}
