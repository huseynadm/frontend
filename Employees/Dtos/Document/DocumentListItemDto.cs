﻿using System;

namespace Azfibernet.Employees.Dtos.Document
{
    public class DocumentListItemDto
    {
        public int Id { get; set; }
        public int DocumentTypeId { get; set; }
        public string DocumentTypeTitle { get; set; }
        public string FileName { get; set; }
        public string FileHash { get; set; }
        public string Description { get; set; }
    }
}
