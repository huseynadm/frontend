﻿using Azfibernet.Shared;
using System.ComponentModel.DataAnnotations;

namespace Azfibernet.Employees.Models
{
    public class ContractType : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        public int? Ordinal { get; set; }
    }
}
