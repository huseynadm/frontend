﻿using Azfibernet.Shared;

namespace Azfibernet.Employees.Models
{
    public class EmployeeDocument : BaseEntity
    {
        public int EmployeeId { get; set; }
        public int EmployeeDocumentTypeId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileHash { get; set; }
        public string FileContentType { get; set; }
        public long FileLength { get; set; }
        public string Description { get; set; }

        public Employee Employee { get; set; }
        public EmployeeDocumentType EmployeeDocumentType { get; set; }
    }
}
