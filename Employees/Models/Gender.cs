﻿using Azfibernet.Shared;
using System.ComponentModel.DataAnnotations;

namespace Azfibernet.Employees.Models
{
    public class Gender : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        public int? Ordinal { get; set; }
    }
}
