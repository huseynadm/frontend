﻿using Azfibernet.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace Azfibernet.Employees.Models
{
    public class EmployeeContract : BaseEntity
    {
        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public int ContractTypeId { get; set; }
        public string Number { get; set; }
        public string WorkbookNumber { get; set; }
        public bool IsPermanent { get; set; }
        public Employee Employee { get; set; }
        public ContractType ContractType { get; set; }
    }
}
