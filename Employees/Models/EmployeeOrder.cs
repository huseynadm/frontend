﻿using Azfibernet.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace Azfibernet.Employees.Models
{
    public class EmployeeOrder : BaseEntity
    {
        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public int OrderTypeId { get; set; }

        [Required]
        public string Number { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        public OrderType OrderType { get; set; }
    }
}
