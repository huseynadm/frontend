﻿using Azfibernet.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace Azfibernet.Employees.Models
{
    public class EmployeeVacationBalance : BaseEntity
    {
        [Required]
        public int EmployeeId { get; set; }

        public DateTime WorkYearStartDate { get; set; }
        public DateTime WorkYearEndDate { get; set; }
        public int MainVacationDays { get; set; }
        public int? AdditionalVacationDays { get; set; }
        public int? UsedVacationDays { get; set; }
    }
}
