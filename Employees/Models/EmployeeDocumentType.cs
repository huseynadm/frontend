﻿using Azfibernet.Shared;
using System.ComponentModel.DataAnnotations;

namespace Azfibernet.Employees.Models
{
    public class EmployeeDocumentType : BaseEntity
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Name { get; set; }

        public string AllowedExtensions { get; set; }

        public EmployeeDocumentType()
        {
        }

        public EmployeeDocumentType(int id, string title, string name, string allowedExtensions)
        {
            Id = id;
            Title = title;
            Name = name;
            AllowedExtensions = allowedExtensions;
        }
    }
}
