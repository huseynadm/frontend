﻿using Azfibernet.Shared;
using System.ComponentModel.DataAnnotations;

namespace Azfibernet.Employees.Models
{
    public class StaffType : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        public int? Ordinal { get; set; }
    }
}
