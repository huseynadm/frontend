﻿using Azfibernet.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace Azfibernet.Employees.Models
{
    public class Employee : BaseEntity
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string MiddleName { get; set; }

        public int GenderId { get; set; }
        public int? MaritalStatusId { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PersonalIdentificationNumber { get; set; }
        public string IdCardSerialNumber { get; set; }
        public string SocialInsuaranceCardNumber { get; set; }
        public string Company { get; set; }
        public bool IsArchived { get; set; }
        public int? UserId { get; set; }
        public int? DepartmentId { get; set; }
        public int? PositionId { get; set; }
        public int? StaffTypeId { get; set; }
        public int? EmploymentTypeId { get; set; }
        public int? WorkPlaceTypeId { get; set; }
        public int? WorkScheduleTypeId { get; set; }
        public string WeeklyWorkHours { get; set; }
        public string Nationality { get; set; }
        public string Citizenship { get; set; }
        public int? EducationDegreeId { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public string CorporationPhoneNumber { get; set; }
        public string InternalPhoneNumber { get; set; }
        public string TotalPastWorkExperience { get; set; }
        public DateTime? HireDate { get; set; }
        public decimal? GrossSalary { get; set; }
        public decimal? NetSalary { get; set; }

        public Department Department { get; set; }
        public EducationDegree EducationDegree { get; set; }
        public EmploymentType EmploymentType { get; set; }
        public StaffType StaffType { get; set; }
        public WorkScheduleType WorkScheduleType { get; set; }
        public WorkPlaceType WorkPlaceType { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
    }
}
