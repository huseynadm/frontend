﻿using Azfibernet.Employees.Dtos;
using Azfibernet.Employees.Dtos.Document;
using Azfibernet.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Azfibernet.Employees
{
    public interface IEmployeeService
    {
        Task<IEnumerable<SelectItemDto>> GetContractTypesAsync();
        Task<IEnumerable<SelectItemDto>> GetDepartmentsAsync();        
        Task<IEnumerable<SelectItemDto>> GetEducationDegreesAsync();
        Task<IEnumerable<EmployeeDocumentTypeDto>> GetDocumentTypesAsync();
        Task<IEnumerable<SelectItemDto>> GetEmploymentTypesAsync();
        Task<IEnumerable<SelectItemDto>> GetGendersAsync();
        Task<IEnumerable<SelectItemDto>> GetMaritalStatusesAsync();
        Task<IEnumerable<SelectItemDto>> GetStaffTypesAsync();
        Task<IEnumerable<SelectItemDto>> GetWorkPlaceTypesAsync();
        Task<IEnumerable<SelectItemDto>> GetWorkScheduleTypesAsync();
        Task<IEnumerable<SelectItemDto>> GetOrderTypesAsync();
        Task<PagedResultDto<EmployeeListItemDto>> GetAllAsync(GetAllEmployeesDto input);        
    }
}
