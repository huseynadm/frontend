﻿using AutoMapper;
using Azfibernet.ApplicationDataContext;
using Azfibernet.Employees.Dtos;
using Azfibernet.Employees.Dtos.Document;
using Azfibernet.Employees.Models;
using Azfibernet.Shared;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Azfibernet.Employees
{
    public class EmployeeService : IEmployeeService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        private IHostingEnvironment _hostingEnvironment;
        //private readonly IHttpClientFactory _httpClientFactory;
        /*private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly NfsConfig _nfsConfig;
        public IConfiguration _config { get; set; }*/

        public EmployeeService(
            DataContext dataContext,
            IMapper mapper,
            IHostingEnvironment hostingEnvironment
            //IHttpClientFactory httpClientFactory
            /*IHttpContextAccessor httpContextAccessor,
            IOptions<NfsConfig> nfsConfig,
            IConfiguration config*/)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
            //_httpClientFactory = httpClientFactory;
            /*_httpContextAccessor = httpContextAccessor;
            _nfsConfig = nfsConfig.Value;
            _config = config;*/
        }

        public async Task<PagedResultDto<EmployeeListItemDto>> GetAllAsync(GetAllEmployeesDto input)
        {
            var query = CreateFilteredQuery(input);

            int totalCount = await query.CountAsync();

            if (totalCount == 0)
            {
                return new PagedResultDto<EmployeeListItemDto>(totalCount, new List<EmployeeListItemDto>());
            }

            query = ApplySorting(query, input);
            query = ApplyPaging(query, input);

            List<EmployeeListItemDto> employees = _mapper.Map<List<EmployeeListItemDto>>(await query.ToListAsync());

            //IEnumerable<PositionDto> positions = await GetPositions(employees.Where(e => e.PositionId.HasValue).Select(d => d.PositionId.Value).Distinct().ToArray());

            //employees.ForEach(x => x.Position = positions.FirstOrDefault(z => z.Id == x.PositionId)?.Name);

            return new PagedResultDto<EmployeeListItemDto>(
                totalCount,
                employees);
        }

        public async Task<IEnumerable<SelectItemDto>> GetContractTypesAsync()
        {
            var contractTypes = await _dataContext.ContractTypes.OrderBy(e => e.Ordinal).Select(e => new SelectItemDto()
            {
                Id = e.Id,
                Value = e.Name
            }).ToListAsync();

            return contractTypes;
        }

        public async Task<IEnumerable<SelectItemDto>> GetDepartmentsAsync()
        {
            var departments = await _dataContext.Departments.OrderBy(e => e.Ordinal).Select(e => new SelectItemDto()
            {
                Id = e.Id,
                Value = e.Name
            }).ToListAsync();

            return departments;
        }

        public async Task<IEnumerable<EmployeeDocumentTypeDto>> GetDocumentTypesAsync()
        {
            var documentTypes = await _dataContext.EmployeeDocumentTypes.OrderBy(e => e.Id).ToListAsync();

            return _mapper.Map<List<EmployeeDocumentTypeDto>>(documentTypes);
        }

        public async Task<IEnumerable<SelectItemDto>> GetEducationDegreesAsync()
        {
            var educationDegrees = await _dataContext.EducationDegrees.OrderBy(e => e.Ordinal).Select(e => new SelectItemDto()
            {
                Id = e.Id,
                Value = e.Name
            }).ToListAsync();
            return educationDegrees;
        }

        public async Task<IEnumerable<SelectItemDto>> GetEmploymentTypesAsync()
        {
            var employmentTypes = await _dataContext.EmploymentTypes.OrderBy(e => e.Ordinal).Select(e => new SelectItemDto()
            {
                Id = e.Id,
                Value = e.Name
            }).ToListAsync();

            return employmentTypes;
        }

        public async Task<IEnumerable<SelectItemDto>> GetGendersAsync()
        {
            var genders = await _dataContext.Genders.OrderBy(e => e.Ordinal).Select(e => new SelectItemDto()
            {
                Id = e.Id,
                Value = e.Name
            }).ToListAsync();

            return genders;
        }

        public async Task<IEnumerable<SelectItemDto>> GetMaritalStatusesAsync()
        {
            var maritalStatuses = await _dataContext.MaritalStatuses.OrderBy(e => e.Ordinal).Select(e => new SelectItemDto()
            {
                Id = e.Id,
                Value = e.Name
            }).ToListAsync();

            return maritalStatuses;
        }

        public async Task<IEnumerable<SelectItemDto>> GetOrderTypesAsync()
        {
            var orderTypes = await _dataContext.OrderTypes.OrderBy(e => e.Ordinal).Select(e => new SelectItemDto()
            {
                Id = e.Id,
                Value = e.Name
            }).ToListAsync();

            return orderTypes;
        }

        public async Task<IEnumerable<SelectItemDto>> GetStaffTypesAsync()
        {
            var staffTypes = await _dataContext.StaffTypes.OrderBy(e => e.Ordinal).Select(e => new SelectItemDto()
            {
                Id = e.Id,
                Value = e.Name
            }).ToListAsync();

            return staffTypes;
        }

        public async Task<IEnumerable<SelectItemDto>> GetWorkPlaceTypesAsync()
        {
            var workPlaceTypes = await _dataContext.WorkPlaceTypes.OrderBy(e => e.Ordinal).Select(e => new SelectItemDto()
            {
                Id = e.Id,
                Value = e.Name
            }).ToListAsync();

            return workPlaceTypes;
        }

        public async Task<IEnumerable<SelectItemDto>> GetWorkScheduleTypesAsync()
        {
            var workScheduleTypes = await _dataContext.WorkScheduleTypes.OrderBy(e => e.Ordinal).Select(e => new SelectItemDto()
            {
                Id = e.Id,
                Value = e.Name
            }).ToListAsync();

            return workScheduleTypes;
        }

        private IQueryable<Employee> CreateFilteredQuery(GetAllEmployeesDto input)
        {
            IQueryable<Employee> query = _dataContext.Employees.Include(e => e.Department).AsQueryable();

            if (!string.IsNullOrEmpty(input.FullName))
            {
                query = query.Where(r => EF.Functions.ILike(r.FirstName + " " + r.LastName, $"%{input.FullName}%"));
            }

            if (input.DepartmentId.HasValue)
            {
                query = query.Where(r => r.DepartmentId == input.DepartmentId);
            }

            if (input.WorkScheduleTypeId.HasValue)
            {
                query = query.Where(r => r.WorkScheduleTypeId == input.WorkScheduleTypeId);
            }

            if (input.EmploymentTypeId.HasValue)
            {
                query = query.Where(r => r.EmploymentTypeId == input.EmploymentTypeId);
            }

            if (input.StaffTypeId.HasValue)
            {
                query = query.Where(r => r.StaffTypeId == input.StaffTypeId);
            }

            if (input.WorkPlaceTypeId.HasValue)
            {
                query = query.Where(r => r.WorkPlaceTypeId == input.WorkPlaceTypeId);
            }

            if (input.IsArchived.HasValue)
            {
                query = query.Where(r => r.IsArchived == input.IsArchived);
            }

            if (input.MinAge.HasValue)
            {
                query = query.Where(r => r.BirthDate.Value < DateTime.Today.AddYears((-1) * input.MinAge.Value));
            }

            if (input.MaxAge.HasValue)
            {
                query = query.Where(r => r.BirthDate.Value > new DateTime(DateTime.Today.Year - input.MaxAge.Value, 1, 1));
            }

            if (input.GenderId.HasValue)
            {
                query = query.Where(r => r.GenderId == input.GenderId);
            }

            return query;
        }

        private IQueryable<Employee> ApplySorting(IQueryable<Employee> query, GetAllEmployeesDto input)
        {
            string sortedBy = !string.IsNullOrEmpty(input.SortedBy) ? input.SortedBy : "Id";

            return input.SortedDesc
                ? query.OrderByDescending(r => EF.Property<object>(r, sortedBy))
                : query.OrderBy(r => EF.Property<object>(r, sortedBy));
        }

        private IQueryable<Employee> ApplyPaging(IQueryable<Employee> query, GetAllEmployeesDto input)
        {
            return input.Limit.HasValue
                ? query.Skip(input.Offset).Take(input.Limit.Value)
                : query.Skip(input.Offset);
        }

    }
}
