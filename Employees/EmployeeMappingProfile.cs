﻿using AutoMapper;
using Azfibernet.Employees.Dtos;
using Azfibernet.Employees.Dtos.Contract;
using Azfibernet.Employees.Dtos.Document;
using Azfibernet.Employees.Dtos.Order;
using Azfibernet.Employees.Dtos.VacationBalance;
using Azfibernet.Employees.Dtos.WorkExperienceAndSalary;
using Azfibernet.Employees.Models;

namespace Azfibernet.Employees
{
    public class EmployeeMappingProfile : Profile
    {
        public EmployeeMappingProfile()
        {
            CreateMap<Employee, EmployeeListItemDto>()
                .ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.Department.Name));

            CreateMap<Employee, EmployeeDetailsDto>()
                .ForMember(dest => dest.MaritalStatus, opt => opt.MapFrom(src => src.MaritalStatus.Name))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Gender.Name))
                .ForMember(dest => dest.EducationDegree, opt => opt.MapFrom(src => src.EducationDegree.Name))
                .ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.Department.Name))
                .ForMember(dest => dest.EmploymentType, opt => opt.MapFrom(src => src.EmploymentType.Name))
                .ForMember(dest => dest.WorkScheduleType, opt => opt.MapFrom(src => src.WorkScheduleType.Name))
                .ForMember(dest => dest.StaffType, opt => opt.MapFrom(src => src.StaffType.Name))
                .ForMember(dest => dest.WorkPlaceType, opt => opt.MapFrom(src => src.WorkPlaceType.Name));

            CreateMap<Employee, EmployeeDto>();

            CreateMap<Employee, WorkExperienceAndSalaryDto>();

            CreateMap<EmployeeContract, ContractListItemDto>()
             .ForMember(dest => dest.ContractType, opt => opt.MapFrom(src => src.ContractType.Name));

            CreateMap<EmployeeOrder, OrderListItemDto>()
             .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.OrderType.Name));

            CreateMap<CreateEmployeeContractDto, EmployeeContract>();

            CreateMap<EmployeeOrder, OrderListItemDto>()
             .ForMember(dest => dest.OrderType, opt => opt.MapFrom(src => src.OrderType.Name));

            CreateMap<EmployeeDocument, DocumentListItemDto>()
                .ForMember(dest => dest.DocumentTypeTitle, opt => opt.MapFrom(src => src.EmployeeDocumentType.Title))
                .ForMember(dest => dest.DocumentTypeId, opt => opt.MapFrom(src => src.EmployeeDocumentTypeId));

            CreateMap<CreateEmployeeContractDto, EmployeeContract>();
            CreateMap<EmployeeVacationBalance, VacationBalanceListItemDto>();
            CreateMap<CreateEmployeeOrderDto, EmployeeOrder>();

            CreateMap<CreateEmployeeVacationBalanceDto, EmployeeVacationBalance>();
            CreateMap<UpdateEmployeeVacationBalanceDto, EmployeeVacationBalance>();
            CreateMap<CreateEmployeeDto, Employee>();
            CreateMap<UpdateEmployeeDto, Employee>();

            CreateMap<UpdateEmployeeWorkExperienceAndSalaryDto, Employee>();

            CreateMap<EmployeeVacationBalance, EmployeeVacationBalanceDto>();

            CreateMap<CreateEmployeeDocumentDto, EmployeeDocument>()
                .ForMember(dest => dest.FileName, opt => opt.MapFrom(src => src.Document.FileName))
                .ForMember(dest => dest.FileLength, opt => opt.MapFrom(src => src.Document.Length))
                .ForMember(dest => dest.FileContentType, opt => opt.MapFrom(src => src.Document.ContentType));

            CreateMap<UpdateEmployeeDocumentDto, EmployeeDocument>();

            CreateMap<EmployeeDocumentType, EmployeeDocumentTypeDto>();

            CreateMap<EmployeeDocument, EmployeeDocumentDto>();

            CreateMap<Employee, EmployeeUserDto>()
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.MobileNumber));

            CreateMap<UpdateEmployeeDto, UpdateUserGeneralInfoDto>();
        }
    }
}
