﻿using AutoMapper;
using Azfibernet.ApplicationDataContext;
using Azfibernet.Inventory.Antennas.Models;
using Azfibernet.Inventory.ATS_UplinkPorts.Models;
using Azfibernet.Inventory.BrxAndAiks.Models;
using Azfibernet.Inventory.Cables.Models;
using Azfibernet.Inventory.CorporateServicesPoints.Models;
using Azfibernet.Inventory.DataCabins.Models;
using Azfibernet.Inventory.Dtos;
using Azfibernet.Inventory.Dtos.Create;
using Azfibernet.Inventory.Dtos.Update;
using Azfibernet.Inventory.Interconnections.Models;
using Azfibernet.Inventory.Lifs.Models;
using Azfibernet.Middlewares.ExceptionMiddlewares;
using Azfibernet.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Azfibernet.Inventory
{
    public class InventoryService : IInventoryService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        public InventoryService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<PagedResultDto<AntennaListItemDto>> GetAllAntennasAsync(GetAllInventoriesDto input)
        {
            var getAllQuery = _dataContext.Antennas.AsQueryable();

            int totalCount = await getAllQuery.CountAsync();

            if (totalCount == 0)
            {
                return new PagedResultDto<AntennaListItemDto>(totalCount, new List<AntennaListItemDto>());
            }

            getAllQuery = input.Limit.HasValue ? getAllQuery.Skip(input.Offset).Take(input.Limit.Value)
                : getAllQuery.Skip(input.Offset);

            var listItems = await getAllQuery.ToListAsync();

            if (!listItems.Any())
            {
                return new PagedResultDto<AntennaListItemDto>(0, null);
            }

            List<AntennaListItemDto> antennasDto = _mapper.Map<List<AntennaListItemDto>>(listItems);

            PagedResultDto<AntennaListItemDto> pagedResultDto = new PagedResultDto<AntennaListItemDto>(totalCount, antennasDto);

            return pagedResultDto;
        }

        public async Task<PagedResultDto<PortListItemDto>> GetAllPortsAsync(GetAllInventoriesDto input)
        {
            var getAllQuery = _dataContext.Ports.AsQueryable();

            int totalCount = await getAllQuery.CountAsync();

            if (totalCount == 0)
            {
                return new PagedResultDto<PortListItemDto>(totalCount, new List<PortListItemDto>());
            }

            getAllQuery = input.Limit.HasValue ? getAllQuery.Skip(input.Offset).Take(input.Limit.Value)
                : getAllQuery.Skip(input.Offset);

            var listItems = await getAllQuery.ToListAsync();

            if (!listItems.Any())
            {
                return new PagedResultDto<PortListItemDto>(0, null);
            }

            List<PortListItemDto> ports = _mapper.Map<List<PortListItemDto>>(listItems);

            PagedResultDto<PortListItemDto> pagedResultDto = new PagedResultDto<PortListItemDto>(totalCount, ports);

            return pagedResultDto;
        }

        public async Task<PagedResultDto<BrxAndAikListItemDto>> GetAllBrxAndAiksAsync(GetAllInventoriesDto input)
        {
            var getAllQuery = _dataContext.BrxAndAiks.AsQueryable();

            int totalCount = await getAllQuery.CountAsync();

            if (totalCount == 0)
            {
                return new PagedResultDto<BrxAndAikListItemDto>(totalCount, new List<BrxAndAikListItemDto>());
            }

            getAllQuery = input.Limit.HasValue ? getAllQuery.Skip(input.Offset).Take(input.Limit.Value)
                : getAllQuery.Skip(input.Offset);

            var listItems = await getAllQuery.ToListAsync();

            if (!listItems.Any())
            {
                return new PagedResultDto<BrxAndAikListItemDto>(0, null);
            }

            List<BrxAndAikListItemDto> brxAndAiks = _mapper.Map<List<BrxAndAikListItemDto>>(listItems);

            PagedResultDto<BrxAndAikListItemDto> pagedResultDto = new PagedResultDto<BrxAndAikListItemDto>(totalCount, brxAndAiks);

            return pagedResultDto;
        }

        public async Task<PagedResultDto<CableListItemDto>> GetAllCablesAsync(GetAllInventoriesDto input)
        {
            var getAllQuery = _dataContext.Cables.AsQueryable();

            int totalCount = await getAllQuery.CountAsync();

            if (totalCount == 0)
            {
                return new PagedResultDto<CableListItemDto>(totalCount, new List<CableListItemDto>());
            }

            getAllQuery = input.Limit.HasValue ? getAllQuery.Skip(input.Offset).Take(input.Limit.Value)
                : getAllQuery.Skip(input.Offset);

            var listItems = await getAllQuery.ToListAsync();

            if (!listItems.Any())
            {
                return new PagedResultDto<CableListItemDto>(0, null);
            }

            List<CableListItemDto> cables = _mapper.Map<List<CableListItemDto>>(listItems);

            PagedResultDto<CableListItemDto> pagedResultDto = new PagedResultDto<CableListItemDto>(totalCount, cables);

            return pagedResultDto;
        }

        public async Task<PagedResultDto<ServicesPointListItemDto>> GetAllServicesPointsAsync(GetAllInventoriesDto input)
        {
            var getAllQuery = _dataContext.ServicesPoints.AsQueryable();

            int totalCount = await getAllQuery.CountAsync();

            if (totalCount == 0)
            {
                return new PagedResultDto<ServicesPointListItemDto>(totalCount, new List<ServicesPointListItemDto>());
            }

            getAllQuery = input.Limit.HasValue ? getAllQuery.Skip(input.Offset).Take(input.Limit.Value)
                : getAllQuery.Skip(input.Offset);

            var listItems = await getAllQuery.ToListAsync();

            if (!listItems.Any())
            {
                return new PagedResultDto<ServicesPointListItemDto>(0, null);
            }

            List<ServicesPointListItemDto> servicesPoints = _mapper.Map<List<ServicesPointListItemDto>>(listItems);

            PagedResultDto<ServicesPointListItemDto> pagedResultDto = new PagedResultDto<ServicesPointListItemDto>(totalCount, servicesPoints);

            return pagedResultDto;
        }

        public async Task<PagedResultDto<DataCabinListItemDto>> GetAllDataCabinsAsync(GetAllInventoriesDto input)
        {
            var getAllQuery = _dataContext.DataCabins.AsQueryable();

            int totalCount = await getAllQuery.CountAsync();

            if (totalCount == 0)
            {
                return new PagedResultDto<DataCabinListItemDto>(totalCount, new List<DataCabinListItemDto>());
            }

            getAllQuery = input.Limit.HasValue ? getAllQuery.Skip(input.Offset).Take(input.Limit.Value)
                : getAllQuery.Skip(input.Offset);

            var listItems = await getAllQuery.ToListAsync();

            if (!listItems.Any())
            {
                return new PagedResultDto<DataCabinListItemDto>(0, null);
            }

            List<DataCabinListItemDto> dataCabins = _mapper.Map<List<DataCabinListItemDto>>(listItems);

            PagedResultDto<DataCabinListItemDto> pagedResultDto = new PagedResultDto<DataCabinListItemDto>(totalCount, dataCabins);

            return pagedResultDto;
        }

        public async Task<PagedResultDto<InterconnectionListItemDto>> GetAllInterconnectionsAsync(GetAllInventoriesDto input)
        {
            var getAllQuery = _dataContext.Interconnections.AsQueryable();

            int totalCount = await getAllQuery.CountAsync();

            if (totalCount == 0)
            {
                return new PagedResultDto<InterconnectionListItemDto>(totalCount, new List<InterconnectionListItemDto>());
            }

            getAllQuery = input.Limit.HasValue ? getAllQuery.Skip(input.Offset).Take(input.Limit.Value)
                : getAllQuery.Skip(input.Offset);

            var listItems = await getAllQuery.ToListAsync();

            if (!listItems.Any())
            {
                return new PagedResultDto<InterconnectionListItemDto>(0, null);
            }

            List<InterconnectionListItemDto> interconnections = _mapper.Map<List<InterconnectionListItemDto>>(listItems);

            PagedResultDto<InterconnectionListItemDto> pagedResultDto = new PagedResultDto<InterconnectionListItemDto>(totalCount, interconnections);

            return pagedResultDto;
        }

        public async Task<PagedResultDto<LifListItemDto>> GetAllLifsAsync(GetAllInventoriesDto input)
        {
            var getAllQuery = _dataContext.Lifs.AsQueryable();

            int totalCount = await getAllQuery.CountAsync();

            if (totalCount == 0)
            {
                return new PagedResultDto<LifListItemDto>(totalCount, new List<LifListItemDto>());
            }

            getAllQuery = input.Limit.HasValue ? getAllQuery.Skip(input.Offset).Take(input.Limit.Value)
                : getAllQuery.Skip(input.Offset);

            var listItems = await getAllQuery.ToListAsync();

            if (!listItems.Any())
            {
                return new PagedResultDto<LifListItemDto>(0, null);
            }

            List<LifListItemDto> lifs = _mapper.Map<List<LifListItemDto>>(listItems);

            PagedResultDto<LifListItemDto> pagedResultDto = new PagedResultDto<LifListItemDto>(totalCount, lifs);

            return pagedResultDto;
        }

        public async Task CreateAntennaAsync(CreateAntennaDto input)
        {
            var antenna = _mapper.Map<Antenna>(input);

            await _dataContext.Antennas.AddAsync(antenna);
            await _dataContext.SaveChangesAsync();
        }

        public async Task CreatePortAsync(CreatePortDto input)
        {
            var port = _mapper.Map<Port>(input);

            await _dataContext.Ports.AddAsync(port);
            await _dataContext.SaveChangesAsync();
        }

        public async Task CreateBrxAndAikAsync(CreateBrxAndAikDto input)
        {
            var brxAndAik = _mapper.Map<BrxAndAik>(input);

            await _dataContext.BrxAndAiks.AddAsync(brxAndAik);
            await _dataContext.SaveChangesAsync();
        }

        public async Task CreateCableAsync(CreateCableDto input)
        {
            var cable = _mapper.Map<Cable>(input);

            await _dataContext.Cables.AddAsync(cable);
            await _dataContext.SaveChangesAsync();
        }

        public async Task CreateServicesPointAsync(CreateServicesPointDto input)
        {
            var servicesPoint = _mapper.Map<ServicesPoint>(input);

            await _dataContext.ServicesPoints.AddAsync(servicesPoint);
            await _dataContext.SaveChangesAsync();
        }

        public async Task CreateDataCabinAsync(CreateDataCabinDto input)
        {
            var dataCabin = _mapper.Map<DataCabin>(input);

            await _dataContext.DataCabins.AddAsync(dataCabin);
            await _dataContext.SaveChangesAsync();
        }

        public async Task CreateInterconnectionAsync(CreateInterconnectionDto input)
        {
            var interconnection = _mapper.Map<Interconnection>(input);

            await _dataContext.Interconnections.AddAsync(interconnection);
            await _dataContext.SaveChangesAsync();
        }

        public async Task CreateLifAsync(CreateLifDto input)
        {
            var lif = _mapper.Map<Lif>(input);

            await _dataContext.Lifs.AddAsync(lif);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<InterconnectionListItemDto> GetInterconnectionByIdAsync(int id)
        {
            var inventory = await _dataContext.Interconnections.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            return _mapper.Map<InterconnectionListItemDto>(inventory);
        }
        public async Task<AntennaListItemDto> GetAntennaByIdAsync(int id)
        {
            var inventory = await _dataContext.Antennas.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            return _mapper.Map<AntennaListItemDto>(inventory);
        }

        public async Task<PortListItemDto> GetPortByIdAsync(int id)
        {
            var inventory = await _dataContext.Ports.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            return _mapper.Map<PortListItemDto>(inventory);
        }

        public async Task<BrxAndAikListItemDto> GetBrxAndAikByIdAsync(int id)
        {
            var inventory = await _dataContext.BrxAndAiks.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            return _mapper.Map<BrxAndAikListItemDto>(inventory);
        }

        public async Task<CableListItemDto> GetCableByIdAsync(int id)
        {
            var inventory = await _dataContext.Cables.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            return _mapper.Map<CableListItemDto>(inventory);
        }

        public async Task<ServicesPointListItemDto> GetServicesPointByIdAsync(int id)
        {
            var inventory = await _dataContext.ServicesPoints.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            return _mapper.Map<ServicesPointListItemDto>(inventory);
        }

        public async Task<DataCabinListItemDto> GetDataCabinByIdAsync(int id)
        {
            var inventory = await _dataContext.DataCabins.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            return _mapper.Map<DataCabinListItemDto>(inventory);
        }

        public async Task<LifListItemDto> GetLifByIdAsync(int id)
        {
            var inventory = await _dataContext.Lifs.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            return _mapper.Map<LifListItemDto>(inventory);
        }

        public async Task ChangeInterconnectionStatusAsync(int id)
        {
            var inventory = await _dataContext.Interconnections.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            inventory.IsActive = !inventory.IsActive;
            if (inventory.IsActive is false)
            {
                inventory.DeactivatedDate = DateTime.Now;
            }

            _dataContext.Interconnections.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task ChangeAntennaStatusAsync(int id)
        {
            var inventory = await _dataContext.Antennas.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            inventory.IsActive = !inventory.IsActive;
            if (inventory.IsActive is false)
            {
                inventory.DeactivatedDate = DateTime.Now;
            }

            _dataContext.Antennas.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task ChangePortStatusAsync(int id)
        {
            var inventory = await _dataContext.Ports.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            inventory.IsActive = !inventory.IsActive;
            if (inventory.IsActive is false)
            {
                inventory.DeactivatedDate = DateTime.Now;
            }

            _dataContext.Ports.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task ChangeBrxAndAikStatusAsync(int id)
        {
            var inventory = await _dataContext.BrxAndAiks.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            inventory.IsActive = !inventory.IsActive;
            if (inventory.IsActive is false)
            {
                inventory.DeactivatedDate = DateTime.Now;
            }

            _dataContext.BrxAndAiks.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task ChangeCableStatusAsync(int id)
        {
            var inventory = await _dataContext.Cables.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            inventory.IsActive = !inventory.IsActive;
            if (inventory.IsActive is false)
            {
                inventory.DeactivatedDate = DateTime.Now;
            }

            _dataContext.Cables.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task ChangeServicesPointStatusAsync(int id)
        {
            var inventory = await _dataContext.ServicesPoints.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            inventory.IsActive = !inventory.IsActive;
            if (inventory.IsActive is false)
            {
                inventory.DeactivatedDate = DateTime.Now;
            }

            _dataContext.ServicesPoints.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task ChangeDataCabinStatusAsync(int id)
        {
            var inventory = await _dataContext.DataCabins.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            inventory.IsActive = !inventory.IsActive;
            if (inventory.IsActive is false)
            {
                inventory.DeactivatedDate = DateTime.Now;
            }

            _dataContext.DataCabins.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task ChangeLifStatusAsync(int id)
        {
            var inventory = await _dataContext.Lifs.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            inventory.IsActive = !inventory.IsActive;
            if (inventory.IsActive is false)
            {
                inventory.DeactivatedDate = DateTime.Now;
            }

            _dataContext.Lifs.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateInterconnectionAsync(int id, UpdateInterconnectionDto input)
        {
            var inventory = await _dataContext.Interconnections.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _mapper.Map(input, inventory);
            _dataContext.Interconnections.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateAntennaAsync(int id, UpdateAntennaDto input)
        {
            var inventory = await _dataContext.Antennas.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _mapper.Map(input, inventory);
            _dataContext.Antennas.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdatePortAsync(int id, UpdatePortDto input)
        {
            var inventory = await _dataContext.Ports.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _mapper.Map(input, inventory);
            _dataContext.Ports.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateBrxAndAikAsync(int id, UpdateBrxAndAikDto input)
        {
            var inventory = await _dataContext.BrxAndAiks.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _mapper.Map(input, inventory);
            _dataContext.BrxAndAiks.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateCableAsync(int id, UpdateCableDto input)
        {
            var inventory = await _dataContext.Cables.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _mapper.Map(input, inventory);
            _dataContext.Cables.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }
        
        public async Task UpdateServicesPointAsync(int id, UpdateServicesPointDto input)
        {
            var inventory = await _dataContext.ServicesPoints.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _mapper.Map(input, inventory);
            _dataContext.ServicesPoints.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateDataCabinAsync(int id, UpdateDataCabinDto input)
        {
            var inventory = await _dataContext.DataCabins.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _mapper.Map(input, inventory);
            _dataContext.DataCabins.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateLifAsync(int id, UpdateLifDto input)
        {
            var inventory = await _dataContext.Lifs.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _mapper.Map(input, inventory);
            _dataContext.Lifs.Update(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteInterconnectionAsync(int id)
        {
            var inventory = await _dataContext.Interconnections.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _dataContext.Interconnections.Remove(inventory);
            await _dataContext.SaveChangesAsync();
        }        

        public async Task DeleteAntennaAsync(int id)
        {
            var inventory = await _dataContext.Antennas.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _dataContext.Antennas.Remove(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeletePortAsync(int id)
        {
            var inventory = await _dataContext.Ports.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _dataContext.Ports.Remove(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteBrxAndAikAsync(int id)
        {
            var inventory = await _dataContext.BrxAndAiks.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _dataContext.BrxAndAiks.Remove(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteCableAsync(int id)
        {
            var inventory = await _dataContext.Cables.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _dataContext.Cables.Remove(inventory);
            await _dataContext.SaveChangesAsync();
        }
        
        public async Task DeleteServicesPointAsync(int id)
        {
            var inventory = await _dataContext.ServicesPoints.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _dataContext.ServicesPoints.Remove(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteDataCabinAsync(int id)
        {
            var inventory = await _dataContext.DataCabins.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _dataContext.DataCabins.Remove(inventory);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteLifAsync(int id)
        {
            var inventory = await _dataContext.Lifs.FirstOrDefaultAsync(x => x.Id == id);

            if (inventory == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Inventory with id = {id} wasn't found."
                };
            }

            _dataContext.Lifs.Remove(inventory);
            await _dataContext.SaveChangesAsync();
        }
    }
}
