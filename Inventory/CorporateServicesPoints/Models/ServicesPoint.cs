﻿using Azfibernet.Shared;
using System;

namespace Azfibernet.Inventory.CorporateServicesPoints.Models
{
    public class ServicesPoint : BaseEntity
    {
        public string SideOrAddress { get; set; }
        public string Location { get; set; }
        public string LastEquipment { get; set; }
        public string LastEquipmentIP { get; set; }
        public string MngVLAN { get; set; }
        public string MngIP { get; set; }
        public string ServiceVLAN { get; set; }
        public string Uplink { get; set; }        
        public string Note { get; set; }
        public bool IsActive { get; set; } = true;
        public DateTime? DeactivatedDate { get; set; }
    }
}
