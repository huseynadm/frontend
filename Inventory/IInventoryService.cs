﻿using Azfibernet.Inventory.Dtos;
using Azfibernet.Inventory.Dtos.Create;
using Azfibernet.Inventory.Dtos.Update;
using Azfibernet.Shared;
using System.Threading.Tasks;

namespace Azfibernet.Inventory
{
    public interface IInventoryService
    {
        Task<PagedResultDto<AntennaListItemDto>> GetAllAntennasAsync(GetAllInventoriesDto input);
        Task<PagedResultDto<PortListItemDto>> GetAllPortsAsync(GetAllInventoriesDto input);
        Task<PagedResultDto<BrxAndAikListItemDto>> GetAllBrxAndAiksAsync(GetAllInventoriesDto input);
        Task<PagedResultDto<CableListItemDto>> GetAllCablesAsync(GetAllInventoriesDto input);
        Task<PagedResultDto<ServicesPointListItemDto>> GetAllServicesPointsAsync(GetAllInventoriesDto input);
        Task<PagedResultDto<DataCabinListItemDto>> GetAllDataCabinsAsync(GetAllInventoriesDto input);
        Task<PagedResultDto<InterconnectionListItemDto>> GetAllInterconnectionsAsync(GetAllInventoriesDto input);
        Task<PagedResultDto<LifListItemDto>> GetAllLifsAsync(GetAllInventoriesDto input);
        Task CreateAntennaAsync(CreateAntennaDto input);
        Task CreatePortAsync(CreatePortDto input);
        Task CreateBrxAndAikAsync(CreateBrxAndAikDto input);
        Task CreateCableAsync(CreateCableDto input);
        Task CreateServicesPointAsync(CreateServicesPointDto input);
        Task CreateDataCabinAsync(CreateDataCabinDto input);
        Task CreateInterconnectionAsync(CreateInterconnectionDto input);
        Task CreateLifAsync(CreateLifDto input);
        Task<InterconnectionListItemDto> GetInterconnectionByIdAsync(int id);
        Task<AntennaListItemDto> GetAntennaByIdAsync(int id);
        Task<PortListItemDto> GetPortByIdAsync(int id);
        Task<BrxAndAikListItemDto> GetBrxAndAikByIdAsync(int id);
        Task<CableListItemDto> GetCableByIdAsync(int id);
        Task<ServicesPointListItemDto> GetServicesPointByIdAsync(int id);
        Task<DataCabinListItemDto> GetDataCabinByIdAsync(int id);
        Task<LifListItemDto> GetLifByIdAsync(int id);
        Task ChangeInterconnectionStatusAsync(int id);
        Task ChangeAntennaStatusAsync(int id);
        Task ChangePortStatusAsync(int id);
        Task ChangeBrxAndAikStatusAsync(int id);
        Task ChangeCableStatusAsync(int id);
        Task ChangeServicesPointStatusAsync(int id);
        Task ChangeDataCabinStatusAsync(int id);
        Task ChangeLifStatusAsync(int id);
        Task UpdateInterconnectionAsync(int id, UpdateInterconnectionDto input);
        Task UpdateAntennaAsync(int id, UpdateAntennaDto input);
        Task UpdatePortAsync(int id, UpdatePortDto input);
        Task UpdateBrxAndAikAsync(int id, UpdateBrxAndAikDto input);
        Task UpdateCableAsync(int id, UpdateCableDto input);
        Task UpdateServicesPointAsync(int id, UpdateServicesPointDto input);
        Task UpdateDataCabinAsync(int id, UpdateDataCabinDto input);
        Task UpdateLifAsync(int id, UpdateLifDto input);
        Task DeleteInterconnectionAsync(int id);
        Task DeleteAntennaAsync(int id);
        Task DeletePortAsync(int id);
        Task DeleteBrxAndAikAsync(int id);
        Task DeleteCableAsync(int id);
        Task DeleteServicesPointAsync(int id);
        Task DeleteDataCabinAsync(int id);
        Task DeleteLifAsync(int id);
    }
}
