﻿using Azfibernet.Shared;
using System;

namespace Azfibernet.Inventory.Cables.Models
{
    public class Cable : BaseEntity
    {
        public string Decree { get; set; }
        public string InfrastructureOwner { get; set; }
        public string Contract { get; set; }
        public DateTime? ContractDate { get; set; }
        public string ProjectCompany { get; set; }
        public string ProjectId { get; set; }
        public string SubContracter { get; set; }
        public string SubContractId { get; set; }
        public DateTime? ProvidingDate { get; set; }
        public string CableType { get; set; }
        public int? CableSize { get; set; }
        public string CableLength { get; set; }
        public string ASideOrAddress { get; set; }
        public string ALocation { get; set; }
        public string BSideOrAddress { get; set; }
        public string BLocation { get; set; }
        public int? TerminatedFibers { get; set; }
        public int? ClosureNumber{ get; set; }
        public string Note{ get; set; }
        public bool IsActive { get; set; } = true;
        public DateTime? DeactivatedDate { get; set; }
    }
}
