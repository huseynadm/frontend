﻿using Azfibernet.Common.ActionFilters;
using Azfibernet.Inventory.Dtos;
using Azfibernet.Inventory.Dtos.Create;
using Azfibernet.Inventory.Dtos.Update;
using Azfibernet.Shared;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Azfibernet.Inventory
{
    public class InventoryController : BaseController
    {
        private readonly IInventoryService _inventoryService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public InventoryController(IInventoryService inventoryService, IHttpContextAccessor httpContextAccessor)
        {
            _inventoryService = inventoryService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("inventory/antennas")]
        [CustomAuthorize("Antenna:list")]
        public async Task<IActionResult> GetAllAntennasAsync([FromQuery] GetAllInventoriesDto input)
        {
            return Ok(await _inventoryService.GetAllAntennasAsync(input));
        }

        [HttpGet("inventory/ports")]
        [CustomAuthorize("Port:list")]
        public async Task<IActionResult> GetAllPortsAsync([FromQuery] GetAllInventoriesDto input)
        {
            return Ok(await _inventoryService.GetAllPortsAsync(input));
        }

        [HttpGet("inventory/brx-and-aiks")]
        [CustomAuthorize("BrxAndAik:list")]
        public async Task<IActionResult> GetAllBrxAndAiksAsync([FromQuery] GetAllInventoriesDto input)
        {
            return Ok(await _inventoryService.GetAllBrxAndAiksAsync(input));
        }

        [HttpGet("inventory/cables")]
        [CustomAuthorize("Cable:list")]
        public async Task<IActionResult> GetAllCablesAsync([FromQuery] GetAllInventoriesDto input)
        {
            return Ok(await _inventoryService.GetAllCablesAsync(input));
        }

        [HttpGet("inventory/services-points")]
        [CustomAuthorize("ServicesPoint:list")]
        public async Task<IActionResult> GetAllServicesPointsAsync([FromQuery] GetAllInventoriesDto input)
        {
            return Ok(await _inventoryService.GetAllServicesPointsAsync(input));
        }

        [HttpGet("inventory/data-cabins")]
        [CustomAuthorize("DataCabin:list")]
        public async Task<IActionResult> GetAllDataCabinsAsync([FromQuery] GetAllInventoriesDto input)
        {
            return Ok(await _inventoryService.GetAllDataCabinsAsync(input));
        }

        [HttpGet("inventory/interconnections")]
        [CustomAuthorize("Interconnection:list")]
        public async Task<IActionResult> GetAllInterconnectionsAsync([FromQuery] GetAllInventoriesDto input)
        {
            return Ok(await _inventoryService.GetAllInterconnectionsAsync(input));
        }

        [HttpGet("inventory/lifs")]
        [CustomAuthorize("Lif:list")]
        public async Task<IActionResult> GetAllLifsAsync([FromQuery] GetAllInventoriesDto input)
        {
            return Ok(await _inventoryService.GetAllLifsAsync(input));
        }

        [HttpPost("inventory/antennas")]
        [CustomAuthorize("Antenna:add")]
        public async Task<IActionResult> CreateAntennaAsync([FromBody] CreateAntennaDto input)
        {
            await _inventoryService.CreateAntennaAsync(input);
            return Ok();
        }

        [HttpPost("inventory/ports")]
        [CustomAuthorize("Port:add")]
        public async Task<IActionResult> CreatePortAsync([FromBody] CreatePortDto input)
        {
            await _inventoryService.CreatePortAsync(input);
            return Ok();
        }

        [HttpPost("inventory/brx-and-aiks")]
        [CustomAuthorize("BrxAndAik:add")]
        public async Task<IActionResult> CreateBrxAndAikAsync([FromBody] CreateBrxAndAikDto input)
        {
            await _inventoryService.CreateBrxAndAikAsync(input);
            return Ok();
        }

        [HttpPost("inventory/cables")]
        [CustomAuthorize("Cable:add")]
        public async Task<IActionResult> CreateCableAsync([FromBody] CreateCableDto input)
        {
            await _inventoryService.CreateCableAsync(input);
            return Ok();
        }

        [HttpPost("inventory/services-points")]
        [CustomAuthorize("ServicesPoint:add")]
        public async Task<IActionResult> CreateServicesPointAsync([FromBody] CreateServicesPointDto input)
        {
            await _inventoryService.CreateServicesPointAsync(input);
            return Ok();
        }

        [HttpPost("inventory/data-cabins")]
        [CustomAuthorize("DataCabin:add")]
        public async Task<IActionResult> CreateDataCabinAsync([FromBody] CreateDataCabinDto input)
        {
            await _inventoryService.CreateDataCabinAsync(input);
            return Ok();
        }

        [HttpPost("inventory/interconnections")]
        [CustomAuthorize("Interconnection:add")]
        public async Task<IActionResult> CreateInterconnectionAsync([FromBody] CreateInterconnectionDto input)
        {
            await _inventoryService.CreateInterconnectionAsync(input);
            return Ok();
        }

        [HttpPost("inventory/lifs")]
        [CustomAuthorize("Lif:add")]
        public async Task<IActionResult> CreateLifAsync([FromBody] CreateLifDto input)
        {
            await _inventoryService.CreateLifAsync(input);
            return Ok();
        }

        [HttpGet("inventory/interconnections/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> GetInterconnectionByIdAsync(int id)
        {
            return Ok(await _inventoryService.GetInterconnectionByIdAsync(id));
        }

        [HttpGet("inventory/antennas/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> GetAntennaByIdAsync(int id)
        {
            return Ok(await _inventoryService.GetAntennaByIdAsync(id));
        }

        [HttpGet("inventory/ports/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> GetPortByIdAsync(int id)
        {
            return Ok(await _inventoryService.GetPortByIdAsync(id));
        }

        [HttpGet("inventory/brx-and-aiks/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> GetBrxAndAikByIdAsync(int id)
        {
            return Ok(await _inventoryService.GetBrxAndAikByIdAsync(id));
        }

        [HttpGet("inventory/cables/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> GetCableByIdAsync(int id)
        {
            return Ok(await _inventoryService.GetCableByIdAsync(id));
        }

        [HttpGet("inventory/services-points/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> GetServicesPointByIdAsync(int id)
        {
            return Ok(await _inventoryService.GetServicesPointByIdAsync(id));
        }

        [HttpGet("inventory/data-cabins/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> GetDataCabinByIdAsync(int id)
        {
            return Ok(await _inventoryService.GetDataCabinByIdAsync(id));
        }

        [HttpGet("inventory/lifs/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> GetLifByIdAsync(int id)
        {
            return Ok(await _inventoryService.GetLifByIdAsync(id));
        }

        [HttpPatch("inventory/interconnections/{id}/status")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> ChangeInterconnectionStatusAsync(int id)
        {
            await _inventoryService.ChangeInterconnectionStatusAsync(id);
            return Ok();
        }

        [HttpPatch("inventory/antennas/{id}/status")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> ChangeAntennaStatusAsync(int id)
        {
            await _inventoryService.ChangeAntennaStatusAsync(id);
            return Ok();
        }

        [HttpPatch("inventory/ports/{id}/status")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> ChangePortStatusAsync(int id)
        {
            await _inventoryService.ChangePortStatusAsync(id);
            return Ok();
        }
        
        [HttpPatch("inventory/brx-and-aiks/{id}/status")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> ChangeBrxAndAikStatusAsync(int id)
        {
            await _inventoryService.ChangeBrxAndAikStatusAsync(id);
            return Ok();
        }

        [HttpPatch("inventory/cables/{id}/status")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> ChangeCableStatusAsync(int id)
        {
            await _inventoryService.ChangeCableStatusAsync(id);
            return Ok();
        }

        [HttpPatch("inventory/services-points/{id}/status")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> ChangeServicesPointStatusAsync(int id)
        {
            await _inventoryService.ChangeServicesPointStatusAsync(id);
            return Ok();
        }

        [HttpPatch("inventory/data-cabins/{id}/status")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> ChangeDataCabinStatusAsync(int id)
        {
            await _inventoryService.ChangeDataCabinStatusAsync(id);
            return Ok();
        }

        [HttpPatch("inventory/lifs/{id}/status")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> ChangeLifStatusAsync(int id)
        {
            await _inventoryService.ChangeLifStatusAsync(id);
            return Ok();
        }

        [HttpPut("inventory/interconnections/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> UpdateInterconnectionAsync(int id, [FromBody] UpdateInterconnectionDto input)
        {
            input.Id = id;

            await _inventoryService.UpdateInterconnectionAsync(id, input);
            return Ok();
        }

        [HttpPut("inventory/antennas/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> UpdateAntennaAsync(int id, [FromBody] UpdateAntennaDto input)
        {
            input.Id = id;

            await _inventoryService.UpdateAntennaAsync(id, input);
            return Ok();
        }

        [HttpPut("inventory/ports/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> UpdatePortAsync(int id, [FromBody] UpdatePortDto input)
        {
            input.Id = id;

            await _inventoryService.UpdatePortAsync(id, input);
            return Ok();
        }

        [HttpPut("inventory/brx-and-aiks/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> UpdateBrxAndAikAsync(int id, [FromBody] UpdateBrxAndAikDto input)
        {
            input.Id = id;

            await _inventoryService.UpdateBrxAndAikAsync(id, input);
            return Ok();
        }

        [HttpPut("inventory/cables/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> UpdateCableAsync(int id, [FromBody] UpdateCableDto input)
        {
            input.Id = id;

            await _inventoryService.UpdateCableAsync(id, input);
            return Ok();
        }

        [HttpPut("inventory/services-points/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> UpdateServicesPointAsync(int id, [FromBody] UpdateServicesPointDto input)
        {
            input.Id = id;

            await _inventoryService.UpdateServicesPointAsync(id, input);
            return Ok();
        }

        [HttpPut("inventory/data-cabins/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> UpdateDataCabinAsync(int id, [FromBody] UpdateDataCabinDto input)
        {
            input.Id = id;

            await _inventoryService.UpdateDataCabinAsync(id, input);
            return Ok();
        }

        [HttpPut("inventory/lifs/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> UpdateLifAsync(int id, [FromBody] UpdateLifDto input)
        {
            input.Id = id;

            await _inventoryService.UpdateLifAsync(id, input);
            return Ok();
        }

        [HttpDelete("inventory/interconnections/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> DeleteInterconnectionAsync(int id)
        {
            await _inventoryService.DeleteInterconnectionAsync(id);
            return Ok();
        }

        [HttpDelete("inventory/antennas/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> DeleteAntennaAsync(int id)
        {
            await _inventoryService.DeleteAntennaAsync(id);
            return Ok();
        }

        [HttpDelete("inventory/ports/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> DeletePortAsync(int id)
        {
            await _inventoryService.DeletePortAsync(id);
            return Ok();
        }

        [HttpDelete("inventory/brx-and-aiks/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> DeleteBrxAndAikAsync(int id)
        {
            await _inventoryService.DeleteBrxAndAikAsync(id);
            return Ok();
        }

        [HttpDelete("inventory/cables/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> DeleteCableAsync(int id)
        {
            await _inventoryService.DeleteCableAsync(id);
            return Ok();
        }

        [HttpDelete("inventory/services-points/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> DeleteServicesPointAsync(int id)
        {
            await _inventoryService.DeleteServicesPointAsync(id);
            return Ok();
        }

        [HttpDelete("inventory/data-cabins/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> DeleteDataCabinAsync(int id)
        {
            await _inventoryService.DeleteDataCabinAsync(id);
            return Ok();
        }

        [HttpDelete("inventory/lifs/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> DeleteLifAsync(int id)
        {
            await _inventoryService.DeleteLifAsync(id);
            return Ok();
        }
    }
}
