﻿using AutoMapper;
using Azfibernet.Inventory.Antennas.Models;
using Azfibernet.Inventory.ATS_UplinkPorts.Models;
using Azfibernet.Inventory.BrxAndAiks.Models;
using Azfibernet.Inventory.Cables.Models;
using Azfibernet.Inventory.CorporateServicesPoints.Models;
using Azfibernet.Inventory.DataCabins.Models;
using Azfibernet.Inventory.Dtos;
using Azfibernet.Inventory.Dtos.Create;
using Azfibernet.Inventory.Dtos.Update;
using Azfibernet.Inventory.Interconnections.Models;
using Azfibernet.Inventory.Lifs.Models;

namespace Azfibernet.Inventory
{
    public class InventoryMappingProfile : Profile
    {
        public InventoryMappingProfile()
        {
            CreateMap<Antenna, AntennaListItemDto>();
            CreateMap<Port, PortListItemDto>();
            CreateMap<BrxAndAik, BrxAndAikListItemDto>();
            CreateMap<Cable, CableListItemDto>();
            CreateMap<ServicesPoint, ServicesPointListItemDto>();
            CreateMap<DataCabin, DataCabinListItemDto>();
            CreateMap<Interconnection, InterconnectionListItemDto>();
            CreateMap<Lif, LifListItemDto>();

            CreateMap<CreateAntennaDto, Antenna>();
            CreateMap<CreatePortDto, Port>();
            CreateMap<CreateBrxAndAikDto, BrxAndAik>();
            CreateMap<CreateCableDto, Cable>();
            CreateMap<CreateServicesPointDto, ServicesPoint>();
            CreateMap<CreateDataCabinDto, DataCabin>();
            CreateMap<CreateInterconnectionDto, Interconnection>();
            CreateMap<CreateLifDto, Lif>();

            CreateMap<UpdateAntennaDto, Antenna>();
            CreateMap<UpdateInterconnectionDto, Interconnection>();
            CreateMap<UpdatePortDto, Port>();
            CreateMap<UpdateBrxAndAikDto, BrxAndAik>();
            CreateMap<UpdateCableDto, Cable>();
            CreateMap<UpdateServicesPointDto, ServicesPoint>();
            CreateMap<UpdateDataCabinDto, DataCabin>();
            CreateMap<UpdateLifDto, Lif>();
        }
    }
}
