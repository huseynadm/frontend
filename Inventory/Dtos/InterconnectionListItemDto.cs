﻿using System;

namespace Azfibernet.Inventory.Dtos
{
    public class InterconnectionListItemDto
    {
        public int Id { get; set; }
        public string Company { get; set; }
        public string BrxOrSrfNumber { get; set; }
        public string Decree { get; set; }
        public string Contract { get; set; }
        public DateTime? ContractDate { get; set; }
        public string Equipment { get; set; }
        public string EquipmentPort { get; set; }
        public string CompanyEquipment { get; set; }
        public string CompanyEquipmentPort { get; set; }
        public string Speed { get; set; }
        public string VLAN { get; set; }
        public string SideOrAddress { get; set; }
        public string Note { get; set; }
        public bool IsActive { get; set; }
        public DateTime? DeactivatedDate { get; set; }
    }
}
