﻿using Azfibernet.Inventory.Dtos.Create;
using System.Text.Json.Serialization;

namespace Azfibernet.Inventory.Dtos.Update
{
    public class UpdateInterconnectionDto : CreateInterconnectionDto
    {
        [JsonIgnore]
        public int Id { get; set; }
    }
}
