﻿using Azfibernet.Inventory.Dtos.Create;
using System.Text.Json.Serialization;

namespace Azfibernet.Inventory.Dtos.Update
{
    public class UpdateLifDto : CreateLifDto
    {
        [JsonIgnore]
        public int Id { get; set; }
    }
}
