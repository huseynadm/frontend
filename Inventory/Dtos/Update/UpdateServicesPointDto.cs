﻿using Azfibernet.Inventory.Dtos.Create;
using System.Text.Json.Serialization;

namespace Azfibernet.Inventory.Dtos.Update
{
    public class UpdateServicesPointDto : CreateServicesPointDto
    {
        [JsonIgnore]
        public int Id { get; set; }
    }
}
