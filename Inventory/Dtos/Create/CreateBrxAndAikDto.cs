﻿using System;

namespace Azfibernet.Inventory.Dtos.Create
{
    public class CreateBrxAndAikDto
    {
        public string Company { get; set; }
        public string Junction { get; set; }
        public string BrxOrAik { get; set; }
        public string BrxOrAikId { get; set; }
        public string Decree { get; set; }
        public string Contract { get; set; }
        public DateTime? ContractDate { get; set; }
        public string ServiceType { get; set; }
        public string Speed { get; set; }
        public string VLAN { get; set; }
        public string SideOrAddress { get; set; }
        public string UplinkPort { get; set; }
        public string UplinkSide { get; set; }
        public string LastPort { get; set; }
        public string Lif { get; set; }
        public string LastEquipment { get; set; }
        public string LastEquipmentIP { get; set; }
        public string Note { get; set; }
    }
}
