﻿using System;

namespace Azfibernet.Inventory.Dtos.Create
{
    public class CreateLifDto
    {
        public string CableOwner { get; set; }
        public string Contract { get; set; }
        public string Annex { get; set; }
        public DateTime? ContractDate { get; set; }
        public string NodeSide { get; set; }
        public string ServiceSide { get; set; }
        public string ServiceSideLocation { get; set; }
        public int? LifCount { get; set; }
        public double? Length { get; set; }
        public double? UnitPrice { get; set; }
        public double? MonthlyFee { get; set; }
        public string Note { get; set; }
    }
}
