﻿using System;

namespace Azfibernet.Inventory.Dtos.Create
{
    public class CreateAntennaDto
    {
        public string Customer { get; set; }
        public string Operator { get; set; }
        public string Frequency { get; set; }
        public string ConnectionType { get; set; }
        public string AAtennaORBaseStation { get; set; }
        public string AAntennaName { get; set; }
        public string AAntennaIP { get; set; }
        public DateTime? AInstallationDate { get; set; }
        public string AAntennaModel { get; set; }
        public string BAtennaORBaseStation { get; set; }
        public string BAntennaName { get; set; }
        public string BAntennaIP { get; set; }
        public DateTime? BInstallationDate { get; set; }
        public string BAntennaModel { get; set; }
        public string Note { get; set; }
    }
}
