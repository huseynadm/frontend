﻿using System;

namespace Azfibernet.Inventory.Dtos.Create
{
    public class CreatePortDto
    {
        public string Company { get; set; }
        public string BrxNumber { get; set; }
        public string Decree { get; set; }
        public string Contract { get; set; }
        public DateTime? ContractDate { get; set; }
        public string Equipment { get; set; }
        public string EquipmentPort { get; set; }
        public string Speed { get; set; }
        public string SideOrAddress { get; set; }
        public string Location { get; set; }
        public string Note { get; set; }
    }
}
