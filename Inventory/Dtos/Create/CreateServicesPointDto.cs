﻿namespace Azfibernet.Inventory.Dtos.Create
{
    public class CreateServicesPointDto
    {
        public string SideOrAddress { get; set; }
        public string Location { get; set; }
        public string LastEquipment { get; set; }
        public string LastEquipmentIP { get; set; }
        public string MngVLAN { get; set; }
        public string MngIP { get; set; }
        public string ServiceVLAN { get; set; }
        public string Uplink { get; set; }
        public string Note { get; set; }
    }
}
