﻿using Azfibernet.Shared;
using System;

namespace Azfibernet.Inventory.DataCabins.Models
{
    public class DataCabin : BaseEntity
    {
        public string Company { get; set; }
        public string Contract { get; set; }
        public string Annex { get; set; }
        public string RegionOrCity { get; set; }
        public DateTime? ContractDate { get; set; }
        public string Side { get; set; }
        public string CabinSize { get; set; }        
        public double? MonthlyFeeWithVAT { get; set; }
        public string Note { get; set; }
        public bool IsActive { get; set; } = true;
        public DateTime? DeactivatedDate { get; set; }
    }
}
