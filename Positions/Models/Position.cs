﻿using Azfibernet.Shared;

namespace Azfibernet.Positions.Models
{
    public class Position : BaseEntity
    {        
        public string Name { get; set; }
    }
}
