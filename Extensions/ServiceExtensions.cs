﻿using AutoMapper;
using Azfibernet.Auth;
using Azfibernet.Employees;
using Azfibernet.Helpers;
using Azfibernet.Helpers.CertificateHelpers;
using Azfibernet.Helpers.JwtHelpers;
using Azfibernet.Helpers.PassWordHelpers;
using Azfibernet.Inventory;
using Azfibernet.Invoices;
using Azfibernet.Invoices.Dtos;
using Azfibernet.Invoices.Validators;
using Azfibernet.Roles;
using Azfibernet.Roles.Dtos;
using Azfibernet.Roles.Validators;
using Azfibernet.Users;
using Azfibernet.Users.Dtos;
using Azfibernet.Users.Validators;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Azfibernet.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServices(this IServiceCollection services, IConfiguration configuration)
        {
            RegisterServices(services, configuration);
            RegisterMappers(services);
            RegisterValidators(services);
            RegisterHelpers(services, configuration);
        }

        private static void RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IEmployeeService, EmployeeService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IInvoiceService, InvoiceService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IInventoryService, InventoryService>();            
        }

        private static void RegisterMappers(IServiceCollection services)
        {
            MapperConfiguration mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new EmployeeMappingProfile());
                mc.AddProfile(new InvoiceMappingProfile());
                mc.AddProfile(new UserMappingProfile());
                mc.AddProfile(new AccountMappingProfile());
                mc.AddProfile(new RoleMappingProfile());
                mc.AddProfile(new InventoryMappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        private static void RegisterHelpers(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ICertificateHelper, CertificateHelper>();
            services.AddTransient<IJwtHelper, JwtHelper>();
            services.AddTransient<IPasswordHelper, PasswordHelper>();

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = configuration.GetValue<string>("CacheSettings:Host");
                options.InstanceName = configuration.GetValue<string>("CacheSettings:InstanceName");
            });
        }

        private static void RegisterValidators(IServiceCollection services)
        {
            services.AddTransient<IValidator<CreateInvoiceDto>, CreateInvoiceValidator>();
            services.AddTransient<IValidator<UpdateInvoiceDto>, UpdateInvoiceValidator>();
            services.AddTransient<IValidator<CreateUserDto>, UserCreateValidator>();
            services.AddTransient<IValidator<UpdateUserDto>, UserUpdateValidator>();
            services.AddTransient<IValidator<ChangeUserPasswordDto>, ChangeUserPasswordValidator>();
            services.AddTransient<IValidator<RoleCreateDto>, RoleCreateValidator>();
            services.AddTransient<IValidator<UpdateRoleDto>, RoleUpdateValidator>();            
        }
    }
}
