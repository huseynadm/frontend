﻿using Azfibernet.Shared;

namespace Azfibernet.Organizers.Models
{
    public class Organizer : BaseEntity
    {
        public int CreatorUserId { get; set; }
        public string Resource { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Url { get; set; }
        public bool IsActive{ get; set; }
        public string Description { get; set; }
    }
}
