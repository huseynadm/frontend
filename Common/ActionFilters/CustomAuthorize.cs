﻿using Azfibernet.ApplicationDataContext;
using Azfibernet.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace Azfibernet.Common.ActionFilters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class CustomAuthorize : AuthorizeAttribute, IAuthorizationFilter
    {
        private readonly string role;

        public CustomAuthorize(string role)
        {
            this.role = role;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (string.IsNullOrEmpty(role))
            {
                return;
            }

            DataContext dataContext = context.HttpContext.RequestServices.GetService<DataContext>();

            int userId = AuthHelper.GetCurrentUserId(context.HttpContext);

            int? roleId = dataContext.Roles.FirstOrDefault(e => e.RoleName == role)?.Id;

            if (!roleId.HasValue)
            {
                throw new Exception("Role not found!");
            }

            bool checkUserRole = dataContext.UserRoles.Any(e => e.UserId == userId && e.RoleId == roleId.Value);

            if (!checkUserRole)
            {
                context.Result = new ForbidResult();
            }
        }
    }
}
