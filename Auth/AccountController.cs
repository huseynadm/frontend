﻿using Azfibernet.Auth.Dtos;
using Azfibernet.Helpers.JwtHelpers;
using Azfibernet.Helpers.PassWordHelpers;
using Azfibernet.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Azfibernet.Auth
{
    public class AccountController : BaseController
    {
        private readonly IJwtHelper _jwtHelper;
        private readonly IAccountService _accountService;
        private readonly IPasswordHelper _passwordHelper;

        public AccountController(IJwtHelper jwtHelper, IAccountService accountService, IPasswordHelper passwordHelper)
        {
            _jwtHelper = jwtHelper;            
            _accountService = accountService;
            _passwordHelper = passwordHelper;
        }

        [HttpPost("sign-in")]
        [AllowAnonymous]
        public async Task<IActionResult> SignInAsync([FromBody] UserSignInDto userSignInDto)
        {
            userSignInDto.Password = _passwordHelper.Encrypt(userSignInDto.Password);
            UserAuthDto userAuthDto = await _accountService.SignInAsync(userSignInDto);

            if (userAuthDto == null)
            {
                return BadRequest("UserName and/or Password is incorrect!");
            }

            string accessToken = _jwtHelper.GenerateAccessToken(userAuthDto.Id);

            string refreshToken = _jwtHelper.GenerateRefreshToken();

            if (userSignInDto.RememberMe)
            {
                refreshToken = _jwtHelper.GenerateRefreshToken();
            }

            await _accountService.UpdateRefreshTokenAsync(userAuthDto.Id, refreshToken);

            return Ok(new UserTokenDto
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken,
                User = userAuthDto
            });
        }

        [HttpPost("refresh-sign-in")]
        [AllowAnonymous]
        public async Task<IActionResult> RefreshSignInAsync([FromBody] UserRefreshSignInDto userRefreshSignInDto)
        {
            UserAuthDto userAuthDto = await _accountService.RefreshSignInAsync(userRefreshSignInDto.RefreshToken);

            if (userAuthDto == null)
            {
                return Unauthorized();
            }

            UserTokenDto userTokenDto = new UserTokenDto()
            {
                AccessToken = _jwtHelper.GenerateAccessToken(userAuthDto.Id),
                RefreshToken = _jwtHelper.GenerateRefreshToken(),
                User = userAuthDto
            };

            await _accountService.UpdateRefreshTokenAsync(userAuthDto.Id, userTokenDto.RefreshToken);

            return Ok(userTokenDto);
        }
    }
}
