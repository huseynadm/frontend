﻿using AutoMapper;
using Azfibernet.Auth.Dtos;
using Azfibernet.Users.Models;
using System.Linq;

namespace Azfibernet.Auth
{
    public class AccountMappingProfile : Profile
    {
        public AccountMappingProfile()
        {
            CreateMap<User, UserAuthDto>()
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName} {src.MiddleName}"))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(e => e.Position.Name))
                .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.UserRoles.Select(ur => ur.Role.RoleName)));
        }
    }
}
