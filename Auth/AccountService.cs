﻿using AutoMapper;
using Azfibernet.ApplicationDataContext;
using Azfibernet.Auth.Dtos;
using Azfibernet.Users.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Azfibernet.Auth
{
    public class AccountService : IAccountService
    {
        private readonly IMapper _mapper;       
        private readonly DataContext _dataContext;
        

        public AccountService(IMapper mapper, DataContext dataContext)
        {
            _mapper = mapper;            
            _dataContext = dataContext;
        }

        public async Task<UserAuthDto> SignInAsync(UserSignInDto userSignInDto)
        {
            User user = await _dataContext.Users.Include(e => e.Position).FirstOrDefaultAsync(e => e.UserName == userSignInDto.UserName && e.IsActive);

            if (user == null)
            {
                return null;
            }

            if (user.PasswordHash != userSignInDto.Password)
            {
                return null;
            }

            UserAuthDto userAuthDto = _mapper.Map<UserAuthDto>(user);

            if (userAuthDto != null)
            {
                userAuthDto.Roles = await _dataContext.UserRoles.Where(e => e.UserId == userAuthDto.Id).Select(e => e.Role.RoleName).ToListAsync();
            }

            return userAuthDto;
        }

        public async Task<UserAuthDto> RefreshSignInAsync(string refreshToken)
        {
            User user = await _dataContext.Users.FirstOrDefaultAsync(e => e.RefreshToken == refreshToken);

            if (user == null)
            {
                return null;
            }

            UserAuthDto userAuthDto = _mapper.Map<UserAuthDto>(user);

            if (userAuthDto != null)
            {
                userAuthDto.Roles = await _dataContext.UserRoles.Where(e => e.UserId == userAuthDto.Id).Select(e => e.Role.RoleName).ToListAsync();
            }

            return userAuthDto;
        }

        public async Task UpdateRefreshTokenAsync(int id, string refreshToken)
        {
            User user = await _dataContext.Users.FindAsync(id);
            user.RefreshToken = refreshToken;
            await _dataContext.SaveChangesAsync();
        }
    }
}
