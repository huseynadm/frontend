﻿using Azfibernet.Auth.Dtos;
using System.Threading.Tasks;

namespace Azfibernet.Auth
{
    public interface IAccountService
    {
        Task<UserAuthDto> SignInAsync(UserSignInDto userSignInDto);
        Task<UserAuthDto> RefreshSignInAsync(string refreshToken);
        Task UpdateRefreshTokenAsync(int id, string refreshToken);
    }
}
