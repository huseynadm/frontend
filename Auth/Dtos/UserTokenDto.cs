﻿namespace Azfibernet.Auth.Dtos
{
    public class UserTokenDto
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public UserAuthDto User { get; set; }
    }
}
