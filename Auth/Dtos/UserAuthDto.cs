﻿using System.Collections.Generic;

namespace Azfibernet.Auth.Dtos
{
    public class UserAuthDto
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }        
        public string EmailAddress { get; set; }
        public string Position { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<string> Roles { get; set; }
    }
}
