﻿namespace Azfibernet.Auth.Dtos
{
    public class UserRefreshSignInDto
    {
        public string RefreshToken { get; set; }
    }
}
