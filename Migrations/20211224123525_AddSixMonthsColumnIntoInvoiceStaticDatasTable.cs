﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Azfibernet.Migrations
{
    public partial class AddSixMonthsColumnIntoInvoiceStaticDatasTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SixMonths",
                table: "InvoiceStaticDatas",
                type: "text",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 40, DateTimeKind.Local).AddTicks(9956));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 42, DateTimeKind.Local).AddTicks(282));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 42, DateTimeKind.Local).AddTicks(311));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 42, DateTimeKind.Local).AddTicks(315));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 42, DateTimeKind.Local).AddTicks(317));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 42, DateTimeKind.Local).AddTicks(326));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SixMonths",
                table: "InvoiceStaticDatas");

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 20, 996, DateTimeKind.Local).AddTicks(5327));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 21, 2, DateTimeKind.Local).AddTicks(5890));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 21, 2, DateTimeKind.Local).AddTicks(5924));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 21, 2, DateTimeKind.Local).AddTicks(5929));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 21, 2, DateTimeKind.Local).AddTicks(5932));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 21, 2, DateTimeKind.Local).AddTicks(5942));
        }
    }
}
