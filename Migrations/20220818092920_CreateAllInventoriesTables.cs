﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Azfibernet.Migrations
{
    public partial class CreateAllInventoriesTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Antennas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Customer = table.Column<string>(type: "text", nullable: true),
                    Operator = table.Column<string>(type: "text", nullable: true),
                    Frequency = table.Column<string>(type: "text", nullable: true),
                    ConnectionType = table.Column<string>(type: "text", nullable: true),
                    AAtennaORBaseStation = table.Column<string>(type: "text", nullable: true),
                    AAntennaName = table.Column<string>(type: "text", nullable: true),
                    AAntennaIP = table.Column<string>(type: "text", nullable: true),
                    AInstallationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    AAntennaModel = table.Column<string>(type: "text", nullable: true),
                    BAtennaORBaseStation = table.Column<string>(type: "text", nullable: true),
                    BAntennaName = table.Column<string>(type: "text", nullable: true),
                    BAntennaIP = table.Column<string>(type: "text", nullable: true),
                    BInstallationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    BAntennaModel = table.Column<string>(type: "text", nullable: true),
                    Note = table.Column<string>(type: "text", nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    DeactivatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Antennas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BrxAndAiks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Company = table.Column<string>(type: "text", nullable: true),
                    Junction = table.Column<string>(type: "text", nullable: true),
                    BrxOrAik = table.Column<string>(type: "text", nullable: true),
                    BrxOrAikId = table.Column<string>(type: "text", nullable: true),
                    Decree = table.Column<string>(type: "text", nullable: true),
                    Contract = table.Column<string>(type: "text", nullable: true),
                    ContractDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    ServiceType = table.Column<string>(type: "text", nullable: true),
                    Speed = table.Column<string>(type: "text", nullable: true),
                    VLAN = table.Column<string>(type: "text", nullable: true),
                    SideOrAddress = table.Column<string>(type: "text", nullable: true),
                    UplinkPort = table.Column<string>(type: "text", nullable: true),
                    UplinkSide = table.Column<string>(type: "text", nullable: true),
                    LastPort = table.Column<string>(type: "text", nullable: true),
                    Lif = table.Column<string>(type: "text", nullable: true),
                    LastEquipment = table.Column<string>(type: "text", nullable: true),
                    LastEquipmentIP = table.Column<string>(type: "text", nullable: true),
                    Note = table.Column<string>(type: "text", nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    DeactivatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrxAndAiks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cables",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Decree = table.Column<string>(type: "text", nullable: true),
                    InfrastructureOwner = table.Column<string>(type: "text", nullable: true),
                    Contract = table.Column<string>(type: "text", nullable: true),
                    ContractDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    ProjectCompany = table.Column<string>(type: "text", nullable: true),
                    ProjectId = table.Column<string>(type: "text", nullable: true),
                    SubContracter = table.Column<string>(type: "text", nullable: true),
                    SubContractId = table.Column<string>(type: "text", nullable: true),
                    ProvidingDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CableType = table.Column<string>(type: "text", nullable: true),
                    CableSize = table.Column<int>(type: "integer", nullable: true),
                    CableLength = table.Column<string>(type: "text", nullable: true),
                    ASideOrAddress = table.Column<string>(type: "text", nullable: true),
                    ALocation = table.Column<string>(type: "text", nullable: true),
                    BSideOrAddress = table.Column<string>(type: "text", nullable: true),
                    BLocation = table.Column<string>(type: "text", nullable: true),
                    TerminatedFibers = table.Column<int>(type: "integer", nullable: true),
                    ClosureNumber = table.Column<int>(type: "integer", nullable: true),
                    Note = table.Column<string>(type: "text", nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    DeactivatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cables", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DataCabins",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Company = table.Column<string>(type: "text", nullable: true),
                    Contract = table.Column<string>(type: "text", nullable: true),
                    Annex = table.Column<string>(type: "text", nullable: true),
                    RegionOrCity = table.Column<string>(type: "text", nullable: true),
                    ContractDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Side = table.Column<string>(type: "text", nullable: true),
                    CabinSize = table.Column<string>(type: "text", nullable: true),
                    MonthlyFeeWithVAT = table.Column<double>(type: "double precision", nullable: true),
                    Note = table.Column<string>(type: "text", nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    DeactivatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataCabins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Interconnections",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Company = table.Column<string>(type: "text", nullable: true),
                    BrxOrSrfNumber = table.Column<string>(type: "text", nullable: true),
                    Decree = table.Column<string>(type: "text", nullable: true),
                    Contract = table.Column<string>(type: "text", nullable: true),
                    ContractDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Equipment = table.Column<string>(type: "text", nullable: true),
                    EquipmentPort = table.Column<string>(type: "text", nullable: true),
                    CompanyEquipment = table.Column<string>(type: "text", nullable: true),
                    CompanyEquipmentPort = table.Column<string>(type: "text", nullable: true),
                    Speed = table.Column<string>(type: "text", nullable: true),
                    VLAN = table.Column<string>(type: "text", nullable: true),
                    SideOrAddress = table.Column<string>(type: "text", nullable: true),
                    Note = table.Column<string>(type: "text", nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    DeactivatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Interconnections", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lifs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CableOwner = table.Column<string>(type: "text", nullable: true),
                    Contract = table.Column<string>(type: "text", nullable: true),
                    Annex = table.Column<string>(type: "text", nullable: true),
                    ContractDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    NodeSide = table.Column<string>(type: "text", nullable: true),
                    ServiceSide = table.Column<string>(type: "text", nullable: true),
                    ServiceSideLocation = table.Column<string>(type: "text", nullable: true),
                    LifCount = table.Column<int>(type: "integer", nullable: true),
                    Length = table.Column<double>(type: "double precision", nullable: true),
                    UnitPrice = table.Column<double>(type: "double precision", nullable: true),
                    MonthlyFee = table.Column<double>(type: "double precision", nullable: true),
                    Note = table.Column<string>(type: "text", nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    DeactivatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lifs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ports",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Company = table.Column<string>(type: "text", nullable: true),
                    BrxNumber = table.Column<string>(type: "text", nullable: true),
                    Decree = table.Column<string>(type: "text", nullable: true),
                    Contract = table.Column<string>(type: "text", nullable: true),
                    ContractDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    Equipment = table.Column<string>(type: "text", nullable: true),
                    EquipmentPort = table.Column<string>(type: "text", nullable: true),
                    Speed = table.Column<string>(type: "text", nullable: true),
                    SideOrAddress = table.Column<string>(type: "text", nullable: true),
                    Location = table.Column<string>(type: "text", nullable: true),
                    Note = table.Column<string>(type: "text", nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    DeactivatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ports", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServicesPoints",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SideOrAddress = table.Column<string>(type: "text", nullable: true),
                    Location = table.Column<string>(type: "text", nullable: true),
                    LastEquipment = table.Column<string>(type: "text", nullable: true),
                    LastEquipmentIP = table.Column<string>(type: "text", nullable: true),
                    MngVLAN = table.Column<string>(type: "text", nullable: true),
                    MngIP = table.Column<string>(type: "text", nullable: true),
                    ServiceVLAN = table.Column<string>(type: "text", nullable: true),
                    Uplink = table.Column<string>(type: "text", nullable: true),
                    Note = table.Column<string>(type: "text", nullable: true),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    DeactivatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServicesPoints", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 13, 29, 19, 634, DateTimeKind.Local).AddTicks(7592));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 13, 29, 19, 636, DateTimeKind.Local).AddTicks(5640));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 13, 29, 19, 636, DateTimeKind.Local).AddTicks(5676));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 13, 29, 19, 636, DateTimeKind.Local).AddTicks(5681));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 13, 29, 19, 636, DateTimeKind.Local).AddTicks(5683));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 8, 18, 13, 29, 19, 636, DateTimeKind.Local).AddTicks(5694));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Antennas");

            migrationBuilder.DropTable(
                name: "BrxAndAiks");

            migrationBuilder.DropTable(
                name: "Cables");

            migrationBuilder.DropTable(
                name: "DataCabins");

            migrationBuilder.DropTable(
                name: "Interconnections");

            migrationBuilder.DropTable(
                name: "Lifs");

            migrationBuilder.DropTable(
                name: "Ports");

            migrationBuilder.DropTable(
                name: "ServicesPoints");

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 42, DateTimeKind.Local).AddTicks(2733));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 43, DateTimeKind.Local).AddTicks(7162));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 43, DateTimeKind.Local).AddTicks(7184));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 43, DateTimeKind.Local).AddTicks(7187));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 43, DateTimeKind.Local).AddTicks(7189));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 43, DateTimeKind.Local).AddTicks(7196));
        }
    }
}
