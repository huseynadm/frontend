﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Azfibernet.Migrations
{
    public partial class AddPropertiesToInvoiceStaticDatasTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EngDate",
                table: "InvoiceStaticDatas",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FullMonths",
                table: "InvoiceStaticDatas",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastMonth",
                table: "InvoiceStaticDatas",
                type: "text",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 20, 996, DateTimeKind.Local).AddTicks(5327));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 21, 2, DateTimeKind.Local).AddTicks(5890));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 21, 2, DateTimeKind.Local).AddTicks(5924));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 21, 2, DateTimeKind.Local).AddTicks(5929));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 21, 2, DateTimeKind.Local).AddTicks(5932));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 20, 46, 21, 2, DateTimeKind.Local).AddTicks(5942));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EngDate",
                table: "InvoiceStaticDatas");

            migrationBuilder.DropColumn(
                name: "FullMonths",
                table: "InvoiceStaticDatas");

            migrationBuilder.DropColumn(
                name: "LastMonth",
                table: "InvoiceStaticDatas");

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 12, 34, 0, 716, DateTimeKind.Local).AddTicks(973));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 12, 34, 0, 717, DateTimeKind.Local).AddTicks(514));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 12, 34, 0, 717, DateTimeKind.Local).AddTicks(535));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 12, 34, 0, 717, DateTimeKind.Local).AddTicks(538));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 12, 34, 0, 717, DateTimeKind.Local).AddTicks(539));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 21, 12, 34, 0, 717, DateTimeKind.Local).AddTicks(544));
        }
    }
}
