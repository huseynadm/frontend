﻿using Azfibernet.Employees.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace Azfibernet.Migrations.Configurations
{
    public class EmployeeDocumentTypeConfiguration : IEntityTypeConfiguration<EmployeeDocumentType>
    {
        public void Configure(EntityTypeBuilder<EmployeeDocumentType> builder)
        {
            var employeeDocumentTypes = new List<EmployeeDocumentType>
            {
                new EmployeeDocumentType(1,"Profil şəkli", "profile_photo", ".png,.jpg,.jpeg"),
                new EmployeeDocumentType(2,"Sertifikatlar", "certificate", ".png,.jpg,.jpeg,.doc,.docx,.pdf,.xlsx,.xls"),
                new EmployeeDocumentType(3,"Sürücülük vəsiqəsi", "driving_license", ".png,.jpg,.jpeg,.doc,.docx,.pdf,.xlsx,.xls"),
                new EmployeeDocumentType(4,"Sağlamlıq haqqında arayış", "health_certificate", ".png,.jpg,.jpeg,.doc,.docx,.pdf,.xlsx,.xls"),
                new EmployeeDocumentType(5,"Hərbi bilet", "military_ticket", ".png,.jpg,.jpeg,.doc,.docx,.pdf,.xlsx,.xls"),
                new EmployeeDocumentType(6,"Başqa", "other", null)
            };

            builder.HasData(employeeDocumentTypes);
        }
    }
}
