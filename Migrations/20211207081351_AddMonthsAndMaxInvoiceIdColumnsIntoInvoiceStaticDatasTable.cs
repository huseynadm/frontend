﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Azfibernet.Migrations
{
    public partial class AddMonthsAndMaxInvoiceIdColumnsIntoInvoiceStaticDatasTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaxInvoiceId",
                table: "InvoiceStaticDatas",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Months",
                table: "InvoiceStaticDatas",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxInvoiceId",
                table: "InvoiceStaticDatas");

            migrationBuilder.DropColumn(
                name: "Months",
                table: "InvoiceStaticDatas");
        }
    }
}
