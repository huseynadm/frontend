﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Azfibernet.Migrations
{
    public partial class AddRefreshTokenColumnAndDropSaltHashColumnIntoUsersTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SaltHash",
                table: "Users",
                newName: "RefreshToken");

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 42, DateTimeKind.Local).AddTicks(2733));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 43, DateTimeKind.Local).AddTicks(7162));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 43, DateTimeKind.Local).AddTicks(7184));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 43, DateTimeKind.Local).AddTicks(7187));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 43, DateTimeKind.Local).AddTicks(7189));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 6, 7, 11, 15, 25, 43, DateTimeKind.Local).AddTicks(7196));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RefreshToken",
                table: "Users",
                newName: "SaltHash");

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 16, 16, 59, 22, 44, DateTimeKind.Local).AddTicks(7847));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 16, 16, 59, 22, 46, DateTimeKind.Local).AddTicks(3894));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 16, 16, 59, 22, 46, DateTimeKind.Local).AddTicks(3983));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 16, 16, 59, 22, 46, DateTimeKind.Local).AddTicks(3987));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 16, 16, 59, 22, 46, DateTimeKind.Local).AddTicks(3990));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 16, 16, 59, 22, 46, DateTimeKind.Local).AddTicks(4002));
        }
    }
}
