﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Azfibernet.Migrations
{
    public partial class AddIsActiveColumnIntoInvoicesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Invoices",
                type: "boolean",
                nullable: false,
                defaultValue: true);

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 8, 18, 11, 7, 877, DateTimeKind.Local).AddTicks(6420));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 8, 18, 11, 7, 878, DateTimeKind.Local).AddTicks(8651));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 8, 18, 11, 7, 878, DateTimeKind.Local).AddTicks(8668));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 8, 18, 11, 7, 878, DateTimeKind.Local).AddTicks(8671));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 8, 18, 11, 7, 878, DateTimeKind.Local).AddTicks(8673));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 8, 18, 11, 7, 878, DateTimeKind.Local).AddTicks(8677));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Invoices");

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 40, DateTimeKind.Local).AddTicks(9956));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 42, DateTimeKind.Local).AddTicks(282));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 42, DateTimeKind.Local).AddTicks(311));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 42, DateTimeKind.Local).AddTicks(315));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 42, DateTimeKind.Local).AddTicks(317));

            migrationBuilder.UpdateData(
                table: "EmployeeDocumentTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 24, 16, 35, 25, 42, DateTimeKind.Local).AddTicks(326));
        }
    }
}
