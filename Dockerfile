FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env

RUN pwd && ls -la

COPY . .


RUN dotnet restore --packages /root/.nuget

RUN dotnet build --no-restore Azfibernet.csproj --output artifact


FROM mcr.microsoft.com/dotnet/aspnet:5.0
RUN pwd && ls -la
COPY  artifact/ .
RUN pwd && ls -la

ENV ASPNETCORE_URLS=http://*:5000
ENV ASPNETCORE_HTTP_PORT=https://*:5001
EXPOSE 5001
EXPOSE 5000

CMD ["dotnet", "Azfibernet.dll"]
