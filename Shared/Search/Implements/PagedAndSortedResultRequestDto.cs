﻿namespace Azfibernet.Shared.Search.Implements
{
    public class PagedAndSortedResultRequestDto : IPagedAndSortedResultRequest
    {
        public bool SortedDesc { get; set; } = true;
        public string SortedBy { get; set; }
        public int Offset { get; set; }
        public int? Limit { get; set; }
    }
}
