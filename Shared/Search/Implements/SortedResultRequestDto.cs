﻿namespace Azfibernet.Shared.Search.Implements
{
    public class SortedResultRequestDto : ISortedResultRequest
    {
        public bool SortedDesc { get; set; } = true;
        public string SortedBy { get; set; }
    }
}
