﻿namespace Azfibernet.Shared.Search.Implements
{
    public class PagedResultRequestDto : IPagedResultRequest
    {
        public int Offset { get; set; }
        public int? Limit { get; set; }
    }
}
