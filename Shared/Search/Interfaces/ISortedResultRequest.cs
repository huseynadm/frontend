﻿namespace Azfibernet.Shared.Search
{
    public interface ISortedResultRequest
    {
        public bool SortedDesc { get; set; }
        public string SortedBy { get; set; }
    }
}
