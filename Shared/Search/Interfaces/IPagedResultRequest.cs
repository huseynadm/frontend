﻿namespace Azfibernet.Shared.Search
{
    public interface IPagedResultRequest
    {
        public int Offset { get; set; }
        public int? Limit { get; set; }
    }
}
