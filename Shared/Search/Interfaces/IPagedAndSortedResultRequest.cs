﻿namespace Azfibernet.Shared.Search
{
    public interface IPagedAndSortedResultRequest : ISortedResultRequest, IPagedResultRequest
    {
    }
}
