﻿using System;

namespace uni_backend.Shared
{
    public interface IUpdateAuditedEntity
    {
        DateTime UpdatedDate { get; set; }
    }
}
