﻿using System;

namespace Azfibernet.Shared
{
    public interface ICreateAuditedEntity
    {
        DateTime CreatedDate { get; set; }
    }
}
