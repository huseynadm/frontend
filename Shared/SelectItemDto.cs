﻿namespace Azfibernet.Shared
{
    public class SelectItemDto
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
