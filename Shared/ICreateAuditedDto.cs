﻿using System;

namespace Azfibernet.Shared
{
    public interface ICreateAuditedDto
    {
        DateTime CreatedDate { get; set; }
    }
}
