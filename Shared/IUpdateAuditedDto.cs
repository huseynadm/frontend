﻿using System;

namespace Azfibernet.Shared
{
    public interface IUpdateAuditedDto
    {
        DateTime UpdatedDate { get; set; }
    }
}
