﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Azfibernet.Shared
{
    [Route("api/v1/")]
    [ApiController]
    [Authorize]
    public abstract class BaseController : ControllerBase
    {

    }
}