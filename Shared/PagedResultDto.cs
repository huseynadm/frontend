﻿using System.Collections.Generic;

namespace Azfibernet.Shared
{
    public class PagedResultDto<T>
    {
        public PagedResultDto(int totalCount, IReadOnlyList<T> items)
        {
            TotalCount = totalCount;
            Items = items;
        }

        public int TotalCount { get; set; }
        public IReadOnlyList<T> Items { get; set; }
    }
}
