﻿using Azfibernet.Employees.Models;
using Azfibernet.Inventory.Antennas.Models;
using Azfibernet.Inventory.ATS_UplinkPorts.Models;
using Azfibernet.Inventory.BrxAndAiks.Models;
using Azfibernet.Inventory.Cables.Models;
using Azfibernet.Inventory.CorporateServicesPoints.Models;
using Azfibernet.Inventory.DataCabins.Models;
using Azfibernet.Inventory.Interconnections.Models;
using Azfibernet.Inventory.Lifs.Models;
using Azfibernet.Invoices;
using Azfibernet.Invoices.Models;
using Azfibernet.Migrations.Configurations;
using Azfibernet.Organizers.Models;
using Azfibernet.Positions.Models;
using Azfibernet.Roles.Models;
using Azfibernet.Users.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Azfibernet.ApplicationDataContext
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EmployeeDocumentTypeConfiguration());
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<ContractType> ContractTypes { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<EducationDegree> EducationDegrees { get; set; }
        public DbSet<MaritalStatus> MaritalStatuses { get; set; }
        public DbSet<EmployeeContract> EmployeeContracts { get; set; }
        public DbSet<EmployeeDocument> EmployeeDocuments { get; set; }
        public DbSet<EmployeeDocumentType> EmployeeDocumentTypes { get; set; }
        public DbSet<OrderType> OrderTypes { get; set; }
        public DbSet<EmployeeOrder> EmployeeOrders { get; set; }
        public DbSet<EmployeeVacationBalance> EmployeeVacationBalances { get; set; }
        public DbSet<EmploymentType> EmploymentTypes { get; set; }
        public DbSet<Gender> Genders { get; set; }        
        public DbSet<StaffType> StaffTypes { get; set; }
        public DbSet<WorkPlaceType> WorkPlaceTypes { get; set; }
        public DbSet<WorkScheduleType> WorkScheduleTypes { get; set; }
        public DbSet<InvoiceStaticData> InvoiceStaticDatas { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Interconnection> Interconnections { get; set; }
        public DbSet<Antenna> Antennas { get; set; }
        public DbSet<Port> Ports { get; set; }
        public DbSet<BrxAndAik> BrxAndAiks { get; set; }
        public DbSet<Cable> Cables { get; set; }
        public DbSet<ServicesPoint> ServicesPoints { get; set; }
        public DbSet<DataCabin> DataCabins { get; set; }
        public DbSet<Lif> Lifs { get; set; }
        //public DbSet<Organizer> Organizers { get; set; }
    }
}
