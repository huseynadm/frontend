﻿namespace Azfibernet.Invoices.Dtos
{
    public class InvoiceListItemDto
    {
        public int Id { get; set; }
        public string InvoiceName { get; set; }
        public string InvoiceCode { get; set; }
        public string InvoiceFilePath { get; set; }
        public string Description { get; set; }
        public bool IsCurrentMonth { get; set; }
        public bool IsSomeMonths { get; set; }
        public bool IsActive { get; set; }
    }
}
