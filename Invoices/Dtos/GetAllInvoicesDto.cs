﻿using Azfibernet.Shared.Search.Implements;

namespace Azfibernet.Invoices.Dtos
{
    public class GetAllInvoicesDto : PagedAndSortedResultRequestDto
    {
        public string InvoiceName { get; set; }
        public string InvoiceCode { get; set; }
        public string Description { get; set; }
        public string InvoiceFilePath { get; set; }
        public bool IsCurrentMonth { get; set; }
        public bool IsSomeMonths { get; set; }
        public bool IsActive { get; set; }
    }
}
