﻿namespace Azfibernet.Invoices.Dtos
{
    public class CreateInvoiceDto
    {
        public string InvoiceName { get; set; }
        public string InvoiceCode { get; set; }
        public string InvoiceFilePath { get; set; }
        public string Description { get; set; }
        public bool IsCurrentMonth { get; set; }
        public bool IsSomeMonths { get; set; }
    } 
}
