﻿namespace Azfibernet.Invoices.Dtos
{
    public class UpdateInvoiceDto : CreateInvoiceDto
    {
        public int Id { get; set; }
    }
}
