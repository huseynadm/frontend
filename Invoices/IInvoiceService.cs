﻿using Azfibernet.Invoices.Dtos;
using Azfibernet.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Azfibernet.Invoices
{
    public interface IInvoiceService
    {
        Task<PagedResultDto<InvoiceListItemDto>> GetAllAsync(GetAllInvoicesDto input);
        Task<InvoiceListItemDto> GetByIdAsync(int id);
        Task<int> GetInvoiceIdAsync();
        Task<InvoiceStaticDataDto> GetStaticDataAsync();
        Task CreateAsync(CreateInvoiceDto input);
        Task UpdateAsync(int id, UpdateInvoiceDto input);
        Task UpdateStaticDataAsync(InvoiceStaticDataDto input);
        Task ChangeStatusAsync(int id);
        Task<(byte[], string fileName)> GenerateInvoice(int id);
        Task DeleteAsync(int id);
    }
}
