﻿using AutoMapper;
using Azfibernet.Invoices.Dtos;
using Azfibernet.Invoices.Models;

namespace Azfibernet.Invoices
{
    public class InvoiceMappingProfile :Profile
    {
        public InvoiceMappingProfile()
        {
            CreateMap<Invoice, InvoiceListItemDto>();            
            CreateMap<CreateInvoiceDto, Invoice>();
            CreateMap<UpdateInvoiceDto, Invoice>();

            CreateMap<InvoiceStaticData, InvoiceStaticDataDto>().ReverseMap();            
        }
    }
}
