﻿using AutoMapper;
using Azfibernet.ApplicationDataContext;
using Azfibernet.Invoices.Dtos;
using Azfibernet.Invoices.Models;
using Azfibernet.Middlewares.ExceptionMiddlewares;
using Azfibernet.Shared;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Azfibernet.Invoices
{
    public class InvoiceService : IInvoiceService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        public InvoiceService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<InvoiceListItemDto> GetByIdAsync(int id)
        {
            var invoice = await _dataContext.Invoices.FirstOrDefaultAsync(x => x.Id == id);

            if (invoice == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Invoice with id = {id} wasn't found."
                };
            }

            return _mapper.Map<InvoiceListItemDto>(invoice);
        }

        public async Task<int> GetInvoiceIdAsync()
        {
            var invoice = await _dataContext.InvoiceStaticDatas.FirstAsync();

            return invoice.InvoiceId;
        }

        public async Task<InvoiceStaticDataDto> GetStaticDataAsync()
        {
            var invoiceStaticData = await _dataContext.InvoiceStaticDatas.FirstAsync();                     

            return _mapper.Map<InvoiceStaticDataDto>(invoiceStaticData);
        }

        public async Task CreateAsync(CreateInvoiceDto input)
        {
            var invoice = _mapper.Map<Invoice>(input);            

            await _dataContext.Invoices.AddAsync(invoice);
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(int id, UpdateInvoiceDto input)
        {
            var invoice = await _dataContext.Invoices.FirstOrDefaultAsync(x => x.Id == id);

            if (invoice == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Invoice with id = {id} wasn't found."
                };
            }

            _mapper.Map(input, invoice);            
            _dataContext.Invoices.Update(invoice);
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateStaticDataAsync(InvoiceStaticDataDto input)
        {
            var invoiceStaticData = await _dataContext.InvoiceStaticDatas.FirstAsync();            

            _mapper.Map(input, invoiceStaticData);
            _dataContext.InvoiceStaticDatas.Update(invoiceStaticData);
            await _dataContext.SaveChangesAsync();
        }

        public async Task ChangeStatusAsync(int id)
        {
            var invoice = await _dataContext.Invoices.FirstOrDefaultAsync(x => x.Id == id);

            if (invoice == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Invoice with id = {id} wasn't found."
                };
            }

            invoice.IsActive = !invoice.IsActive;

            _dataContext.Invoices.Update(invoice);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var invoice = await _dataContext.Invoices.FirstOrDefaultAsync(x => x.Id == id);

            if (invoice == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"Invoice with id = {id} wasn't found."
                };
            }

            _dataContext.Invoices.Remove(invoice);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<(byte[], string fileName)> GenerateInvoice(int id)
        {
            var invoiceData = _dataContext.InvoiceStaticDatas.FirstOrDefault();
            var fileData = _dataContext.Invoices.FirstOrDefault(e => e.Id == id);

            JObject json = new JObject();
            json.Add(new JProperty("InvoiceId", invoiceData.InvoiceId));
            json.Add(new JProperty("Date", invoiceData.Date));
            json.Add(new JProperty("EngDate", invoiceData.EngDate));
            json.Add(new JProperty("Year", invoiceData.Year));
            json.Add(new JProperty("Month", invoiceData.Month));
            json.Add(new JProperty("LastMonth", invoiceData.LastMonth));
            json.Add(new JProperty("Months", invoiceData.Months));            
            json.Add(new JProperty("SixMonths", invoiceData.SixMonths));            
            json.Add(new JProperty("FullMonths", invoiceData.FullMonths));            
            
            if (invoiceData.InvoiceId > invoiceData.MaxInvoiceId)
            {
                invoiceData.MaxInvoiceId = invoiceData.InvoiceId;                
            }

            invoiceData.InvoiceId++;
            _dataContext.SaveChanges();            

            var stringFilePath = $"./data/{fileData.InvoiceName}.docx";

            using (FileStream file = File.Open(stringFilePath, FileMode.Open, FileAccess.Read))
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    await file.CopyToAsync(memoryStream);
                    file.Close();

                    using (WordprocessingDocument wordDocument = WordprocessingDocument.Open(memoryStream, true))
                    {
                        var body = wordDocument.MainDocumentPart.Document.Body;

                        foreach (var item in json)
                        {
                            var a = body.Descendants<Text>();
                            foreach (var text in body.Descendants<Text>())
                            {
                                if (text.Text.Trim() == item.Key)
                                {
                                    text.Text = text.Text.Replace(item.Key, item.Value.ToString());
                                }
                            }
                        }

                        wordDocument.MainDocumentPart.Document.Save();
                    }

                    memoryStream.Seek(0, SeekOrigin.Begin);
                    var documentName = $"Hesab {invoiceData.InvoiceId} {fileData.InvoiceName}";

                    return (memoryStream.ToArray(), string.Concat(documentName, ".docx"));
                }
            }

        }

        public async Task<PagedResultDto<InvoiceListItemDto>> GetAllAsync(GetAllInvoicesDto input)
        {
            var getAllQuery = CreateFilteredQuery(input);   

            int totalCount = await getAllQuery.CountAsync();            

            if (totalCount == 0)
            {
                return new PagedResultDto<InvoiceListItemDto>(totalCount, new List<InvoiceListItemDto>());
            }

            getAllQuery = input.Limit.HasValue? getAllQuery.Skip(input.Offset).Take(input.Limit.Value)
                : getAllQuery.Skip(input.Offset);

            List<Invoice> invoices = await getAllQuery.ToListAsync();

            if (!invoices.Any())
            {
                return new PagedResultDto<InvoiceListItemDto>(0, null);
            }

            List<InvoiceListItemDto> invoicesDto = _mapper.Map<List<InvoiceListItemDto>>(invoices);

            PagedResultDto<InvoiceListItemDto> pagedResultDto = new PagedResultDto<InvoiceListItemDto>(totalCount, invoicesDto);
            return pagedResultDto;
        }

        private IQueryable<Invoice> CreateFilteredQuery(GetAllInvoicesDto input)
        {
            IQueryable<Invoice> query = _dataContext.Invoices.AsQueryable();

            if (!string.IsNullOrEmpty(input.InvoiceName))
            {
                query = query.Where(r => EF.Functions.ILike(r.InvoiceName, $"%{input.InvoiceName}%"));
            }

            if (!string.IsNullOrEmpty(input.InvoiceCode))
            {
                query = query.Where(r => EF.Functions.ILike(r.InvoiceCode, $"%{input.InvoiceCode}%"));
            }

            if (!string.IsNullOrEmpty(input.InvoiceFilePath))
            {
                query = query.Where(r => EF.Functions.ILike(r.InvoiceFilePath, $"%{input.InvoiceFilePath}%"));
            }

            if (!string.IsNullOrEmpty(input.Description))
            {
                query = query.Where(r => EF.Functions.ILike(r.Description, $"%{input.Description}%"));
            }

            return query.OrderBy(e=>e.InvoiceName);
        }
    }
}
