﻿using Azfibernet.Common.ActionFilters;
using Azfibernet.Invoices.Dtos;
using Azfibernet.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Azfibernet.Invoices
{
    public class InvoiceController : BaseController
    {
        private readonly IInvoiceService _invoiceService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public InvoiceController(IInvoiceService invoiceService, IHttpContextAccessor httpContextAccessor)
        {
            _invoiceService = invoiceService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("invoices")]
        [CustomAuthorize("Invoice:list")]
        public async Task<IActionResult> GetAllAsync([FromQuery] GetAllInvoicesDto input)
        {
            return Ok(await _invoiceService.GetAllAsync(input));
        }

        [HttpGet("invoices/{id}/generate-invoice")]
        [CustomAuthorize("Accountant")]
        public async Task<IActionResult> GenerateInvoice(int id)
        {
            (byte[] stream, string fileName) = await _invoiceService.GenerateInvoice(id);
            return File(stream, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", fileName);
        }

        [HttpGet("invoices/{id}")]
        [CustomAuthorize("Accountant")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            return Ok(await _invoiceService.GetByIdAsync(id));
        }

        [HttpGet("invoice-static-datas/invoice-id")]
        [AllowAnonymous]
        public async Task<IActionResult> GetInvoiceIdAsync()
        {
            return Ok(await _invoiceService.GetInvoiceIdAsync());
        }

        [HttpGet("invoice-static-datas")]
        [CustomAuthorize("StaticData")]
        public async Task<IActionResult> GetStaticDataAsync()
        {
            return Ok(await _invoiceService.GetStaticDataAsync());
        }

        [HttpPost("invoices")]
        [CustomAuthorize("Invoice:add")]
        public async Task<IActionResult> CreateAsync([FromBody] CreateInvoiceDto input)
        {
            await _invoiceService.CreateAsync(input);
            return Ok();
        }

        [HttpPut("invoices/{id}")]
        [CustomAuthorize("Accountant")]
        public async Task<IActionResult> UpdateAsync(int id, [FromBody] UpdateInvoiceDto input)
        {
            await _invoiceService.UpdateAsync(id, input);
            return Ok();
        }

        [HttpPatch("invoices/{id}/status")]
        [CustomAuthorize("Accountant")]
        public async Task<IActionResult> ChangeStatusAsync(int id)
        {
            await _invoiceService.ChangeStatusAsync(id);
            return Ok();
        }

        [HttpPut("invoice-static-datas")]
        [CustomAuthorize("StaticData")]
        public async Task<IActionResult> UpdateStaticDataAsync([FromBody] InvoiceStaticDataDto input)
        {
            await _invoiceService.UpdateStaticDataAsync(input);
            return Ok();
        }

        [HttpDelete("invoices/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await _invoiceService.DeleteAsync(id);
            return Ok();
        }
    }
}
