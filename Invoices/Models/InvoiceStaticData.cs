﻿namespace Azfibernet.Invoices
{
    public class InvoiceStaticData
    {        
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public int MaxInvoiceId { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string LastMonth { get; set; }
        public string Months { get; set; }
        public string SixMonths { get; set; }
        public string FullMonths { get; set; }
        public string Date { get; set; }
        public string EngDate { get; set; }
    }
}
