﻿using Azfibernet.Shared;

namespace Azfibernet.Invoices.Models
{
    public class Invoice : BaseEntity
    {
        public string InvoiceName { get; set; }       
        public string InvoiceCode { get; set; }
        public string InvoiceFilePath { get; set; }
        public string Description { get; set; }
        public bool IsCurrentMonth { get; set; }
        public bool IsSomeMonths { get; set; }
        public bool IsActive { get; set; } = true;
    }
}
