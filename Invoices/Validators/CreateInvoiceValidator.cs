﻿using Azfibernet.Invoices.Dtos;
using FluentValidation;


namespace Azfibernet.Invoices.Validators
{
    public class CreateInvoiceValidator : AbstractValidator<CreateInvoiceDto>
    {
        public CreateInvoiceValidator()
        {
            RuleFor(e => e.InvoiceName)
                .NotNull().NotEmpty().WithMessage("Invoice Name is required!");

            RuleFor(e => e.InvoiceCode)
                .NotNull().NotEmpty().WithMessage("Invoice Code is required!");       
        }
    }
}
