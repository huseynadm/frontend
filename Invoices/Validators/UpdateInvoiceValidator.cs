﻿using Azfibernet.Invoices.Dtos;
using FluentValidation;

namespace Azfibernet.Invoices.Validators
{
    public class UpdateInvoiceValidator : AbstractValidator<UpdateInvoiceDto>
    {
        public UpdateInvoiceValidator()
        {
            Include(new CreateInvoiceValidator());
        }
    }
}
