﻿using Azfibernet.Common.ActionFilters;
using Azfibernet.Shared;
using Azfibernet.Users.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Azfibernet.Users
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserController(IUserService userService, IHttpContextAccessor httpContextAccessor)
        {
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("users/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            return Ok(await _userService.GetById(id));
        }

        [HttpGet("users")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> GetAllAsync([FromQuery] GetAllUsersDto getAllUsersDto)
        {
            return Ok(await _userService.GetAllAsync(getAllUsersDto));
        }

        [HttpGet("positions/all")]
        public async Task<IActionResult> GetPositionsAsync()
        {
            return Ok(await _userService.GetPositionsAsync());
        }

        [HttpGet("users/check-username-uniqueness")]
        public async Task<IActionResult> CheckUsernameUniqueness([FromQuery] CheckUsernameUniquenessDto checkUsernameUniquenessDto)
        {
            return Ok(await _userService.CheckUsernameUniquenessAsync(checkUsernameUniquenessDto));
        }

        [HttpPost("users")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> CreateAsync([FromBody] CreateUserDto userCreateDto)
        {
            await _userService.CreateAsync(userCreateDto);
            return Ok();
        }

        [HttpPut("users/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> UpdateAsync(int id, [FromBody] UpdateUserDto updateUserDto)
        {
            updateUserDto.Id = id;
            await _userService.UpdateAsync(id, updateUserDto);
            return Ok();
        }

        [HttpPatch("users/{id}/status")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> ChangeStatusAsync(int id)
        {
            await _userService.ChangeStatusAsync(id);
            return Ok();
        }

        [HttpPatch("users/{id}/password")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> ChangePasswordAsync(int id,
           [FromBody] ChangeUserPasswordDto changeUserPasswordDto)
        {
            await _userService.ChangePasswordAsync(id, changeUserPasswordDto);
            return Ok();
        }

        [HttpPatch("users/reset-password")]
        public async Task<IActionResult> ResetPasswordAsync([FromBody] ResetPasswordDto resetPasswordDto)
        {
            await _userService.ResetPasswordAsync(resetPasswordDto);
            return Ok();
        }
    }
}
