﻿using AutoMapper;
using Azfibernet.Users.Dtos;
using Azfibernet.Users.Models;
using System.Linq;

namespace Azfibernet.Users
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<CreateUserDto, User>()
                .ForMember(d => d.UserRoles, o => o.MapFrom(s => s.RoleIds.Select(t =>
                    new UserRole()
                    {
                        UserId = 0,
                        RoleId = t
                    })));

            CreateMap<User, UserDto>().ForMember(dest => dest.FullName, opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName} {src.MiddleName}"))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(e => e.Position.Name));

            CreateMap<User, UserInfoDto>().ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position.Name))
                .ForMember(dest => dest.RoleIds, opt => opt.MapFrom(src => src.UserRoles.Select(ur => ur.RoleId)));

            CreateMap<User, UserAuthenticatedDto>().ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.UserRoles.Select(ur => ur.RoleId)));

            CreateMap<UpdateUserDto, User>();
        }
    }
}
