﻿using Azfibernet.Roles.Models;
using Azfibernet.Shared;

namespace Azfibernet.Users.Models
{
    public class UserRole : BaseEntity
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public User User { get; set; }
        public Role Role { get; set; }
    }
}
