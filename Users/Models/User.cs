﻿using Azfibernet.Positions.Models;
using Azfibernet.Shared;
using System.Collections.Generic;

namespace Azfibernet.Users.Models
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public int PositionId { get; set; }
        public string PasswordHash { get; set; }
        public bool IsSuperAdmin { get; set; } = false;
        public bool IsActive { get; set; } = true;
        public string RefreshToken { get; set; }
        public Position Position { get; set; }
        public IList<UserRole> UserRoles { get; set; }


    }
}
