﻿
namespace Azfibernet.Users.Dtos
{
    public class CheckUsernameUniquenessDto
    {
        public int? UserId { get; set; }
        public string Username { get; set; }
    }
}
