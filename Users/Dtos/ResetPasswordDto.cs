﻿namespace Azfibernet.Users.Dtos
{
    public class ResetPasswordDto
    {
        public string OldPassword { get; set; }
        public string Password { get; set; }
        public string PasswordRepeat { get; set; }
    }
}
