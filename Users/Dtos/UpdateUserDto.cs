﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Azfibernet.Users.Dtos
{
    public class UpdateUserDto
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public int PositionId { get; set; }
        public IList<int> RoleIds { get; set; }
    }
}
