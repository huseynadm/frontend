﻿using System.Collections.Generic;

namespace Azfibernet.Users.Dtos
{
    public class UserAuthenticatedDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IReadOnlyList<int> Roles { get; set; }
    }
}
