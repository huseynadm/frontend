﻿using System.Collections.Generic;

namespace Azfibernet.Users.Dtos
{
    public class CreateUserDto
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public int PositionId { get; set; }
        public string Password { get; set; }
        public string PasswordRepeat { get; set; }
        public IList<int> RoleIds { get; set; }
    }
}
