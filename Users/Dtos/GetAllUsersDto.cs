﻿using Azfibernet.Shared.Search.Implements;

namespace Azfibernet.Users.Dtos
{
    public class GetAllUsersDto : PagedAndSortedResultRequestDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public int? PositionId { get; set; }
        public int? RoleId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
