﻿namespace Azfibernet.Users.Dtos
{
    public class UserDto
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string Position { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsActive { get; set; }
    }
}
