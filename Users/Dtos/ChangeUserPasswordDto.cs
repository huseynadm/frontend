﻿namespace Azfibernet.Users.Dtos
{
    public class ChangeUserPasswordDto
    {
        public string Password { get; set; }
        public string PasswordRepeat { get; set; }
    }
}
