﻿using Azfibernet.Users.Dtos;
using FluentValidation;
using System.Text.RegularExpressions;

namespace Azfibernet.Users.Validators
{
    public class UserCreateValidator : AbstractValidator<CreateUserDto>
    {
        private readonly IUserService _service;

        public UserCreateValidator(IUserService service)
        {
            _service = service;

            RuleFor(e => e.UserName)
              .NotNull().NotEmpty().WithMessage("UserName is required!")
              .Must((dto, UserName) => _service.CheckUsernameUniquenessAsync(new CheckUsernameUniquenessDto
              {
                  Username = dto.UserName
              }).GetAwaiter().GetResult()).WithMessage("User name is exist")
              .Length(5, 50).WithMessage("User name's length must be minimum 5 and maximum 50 characters")
              .Must(CheckUserNameCharacters).WithMessage("User name can include only lowercase characters,dots and numbers");

            RuleFor(e => e.Password)
                .NotNull().NotEmpty().WithMessage("Password is required!")
                .Must((dto, Password) => dto.Password == dto.PasswordRepeat)
                .WithMessage("Passwords are not match!")
                .Length(6, 20).WithMessage("Passwords' length must be minimum 6 and maximum 20 characters")
                .Must(CheckPasswordCharacters).WithMessage("Passwords are not valid!");

            RuleFor(e => e.PasswordRepeat)
                .NotNull().NotEmpty().WithMessage("Password is required!")
                .Must((dto, PasswordRepeat) => dto.PasswordRepeat == dto.Password)
                .WithMessage("Passwords are not match!")
                .Length(6, 20).WithMessage("Passwords' length must be minimum 6 and maximum 20 characters")
                .Must(CheckPasswordCharacters).WithMessage("Passwords are not valid!");

            RuleFor(e => e.FirstName)
                .NotNull().NotEmpty().WithMessage("First Name is required!");

            RuleFor(e => e.LastName)
                .NotNull().NotEmpty().WithMessage("Last Name is required!");

            RuleFor(e => e.PhoneNumber).Must(CheckPhoneNumberCharacters).WithMessage("Phone Number's length must be 9 numeric characters!").Must(CheckPhoneNumberPrefix).WithMessage("Phone Number's prefix must start with 50,51,55,70,77 or 99 numeric characters!");


            RuleFor(e => e.PositionId)
               .NotNull().NotEmpty().WithMessage("Position is required!");

            RuleFor(e => e.EmailAddress)
                .NotNull().NotEmpty().WithMessage("Email Address is required!")
                .Must(CheckEmailAddressCharacters).WithMessage("Email Address is not valid!");               


            RuleFor(e => e.RoleIds).NotNull().NotEmpty().WithMessage("Roles are required!")
                .ForEach(e => e.Must(i => i > 0).WithMessage("Roles are required!"));
        }
        private bool CheckPhoneNumberCharacters(string phoneNumber)
        {
            return Regex.Matches(phoneNumber, "^[0-9]{9}$").Count > 0;
        }
        private bool CheckPhoneNumberPrefix(string phoneNumber)
        {
            Regex phoneNumberPrefix = new Regex(@"^(50|51|55|70|77|99)([1-9]{1})([0-9]{6})$");
            return phoneNumberPrefix.Matches(phoneNumber).Count > 0;
        }

        private bool CheckUserNameCharacters(string userName)
        {
            Regex UserNameCharacters = new Regex(@"^[a-z]+(\.[a-z]+)*(\d)*$");
            return UserNameCharacters.Matches(userName).Count > 0;
        }

        private bool CheckPasswordCharacters(string password)
        {
            Regex passwordCharacters = new Regex(@"^(?!\.)(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[.#$^+=!*()@%&]).{6,20}$");
            return passwordCharacters.Matches(password).Count > 0;
        }

        private bool CheckEmailAddressCharacters(string emailAddress)
        {
            Regex emailAddressCharacters = new Regex(@"^[^@\s]+@[^@\s]+\.[^@\s]+$");
            return emailAddressCharacters.Matches(emailAddress).Count > 0;
        }
    }
}
