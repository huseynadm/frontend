﻿using Azfibernet.Users.Dtos;
using FluentValidation;
using System.Text.RegularExpressions;

namespace Azfibernet.Users.Validators
{
    public class ChangeUserPasswordValidator : AbstractValidator<ChangeUserPasswordDto>
    {
        public ChangeUserPasswordValidator()
        {
            RuleFor(e => e.Password)
              .NotNull().NotEmpty().WithMessage("Password is required!")
              .Must((dto, Password) => dto.Password == dto.PasswordRepeat).WithMessage("Passwords are not match!")
              .Length(6, 20).WithMessage("Passwords' length must be minimum 6 and maximum 20 characters")
              .Must(CheckPasswordCharacters).WithMessage("Passwords are not valid!");

            RuleFor(e => e.PasswordRepeat)
                .NotNull().NotEmpty().WithMessage("Password is required!")
                .Must((dto, PasswordRepeat) => dto.PasswordRepeat == dto.Password).WithMessage("Passwords are not match!")
                .Length(6, 20).WithMessage("Passwords' length must be minimum 6 and maximum 20 characters")
                .Must(CheckPasswordCharacters).WithMessage("Passwords are not valid!");
        }

        private bool CheckPasswordCharacters(string password)
        {
            Regex passwordCharacters = new Regex(@"^(?!\.)(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[.#$^+=!*()@%&]).{6,20}$");
            return passwordCharacters.Matches(password).Count > 0;
        }
    }
}
