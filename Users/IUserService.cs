﻿using Azfibernet.Shared;
using Azfibernet.Users.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Azfibernet.Users
{
    public interface IUserService
    {
        Task<UserInfoDto> GetById(int id);
        Task<PagedResultDto<UserDto>> GetAllAsync(GetAllUsersDto getAllUsersDto);
        Task<IEnumerable<SelectItemDto>> GetPositionsAsync();
        Task CreateAsync(CreateUserDto userCreateDto);
        Task UpdateAsync(int id, UpdateUserDto updateUserDto);
        Task ChangePasswordAsync(int id, ChangeUserPasswordDto changeUserPasswordDto);
        Task<bool> CheckUsernameUniquenessAsync(CheckUsernameUniquenessDto checkUsernameUniquenessDto);
        Task ChangeStatusAsync(int id);
        Task ResetPasswordAsync(ResetPasswordDto resetPasswordDto);
    }
}
