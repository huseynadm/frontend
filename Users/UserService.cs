﻿using AutoMapper;
using Azfibernet.ApplicationDataContext;
using Azfibernet.Helpers;
using Azfibernet.Helpers.PassWordHelpers;
using Azfibernet.Middlewares.ExceptionMiddlewares;
using Azfibernet.Shared;
using Azfibernet.Users.Dtos;
using Azfibernet.Users.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Azfibernet.Users
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly DataContext _dataContext;
        private readonly IPasswordHelper _passwordHelper;

        public UserService(IMapper mapper, IHttpContextAccessor httpContextAccessor, DataContext dataContext, IPasswordHelper passwordHelper)
        {
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
            _dataContext = dataContext;
            _passwordHelper = passwordHelper;
        }

        public async Task<UserInfoDto> GetById(int id)
        {
            User user = await _dataContext.Users.Include(u => u.UserRoles).Include(e => e.Position).FirstOrDefaultAsync(u => u.Id == id);
            if (user == null)
            {

                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = "No user with id {id} was found."
                };
            }

            return _mapper.Map<UserInfoDto>(user);
        }

        public async Task<PagedResultDto<UserDto>> GetAllAsync(GetAllUsersDto getAllUsersDto)
        {
            IQueryable<User> getAllQuery = _dataContext.Users.Include(u => u.UserRoles).Include(e => e.Position).AsQueryable();

            if (!string.IsNullOrEmpty(getAllUsersDto.FirstName))
            {
                getAllQuery = getAllQuery.Where(r => EF.Functions.ILike(r.FirstName, $"%{getAllUsersDto.FirstName}%"));
            }

            if (!string.IsNullOrEmpty(getAllUsersDto.LastName))
            {
                getAllQuery = getAllQuery.Where(r => EF.Functions.ILike(r.LastName, $"%{getAllUsersDto.LastName}%"));
            }

            if (!string.IsNullOrEmpty(getAllUsersDto.UserName))
            {
                getAllQuery = getAllQuery.Where(r => EF.Functions.ILike(r.UserName, $"%{getAllUsersDto.UserName}%"));
            }

            if (!string.IsNullOrEmpty(getAllUsersDto.EmailAddress))
            {
                getAllQuery = getAllQuery.Where(r =>
                    EF.Functions.ILike(r.EmailAddress, $"%{getAllUsersDto.EmailAddress}%"));
            }

            if (getAllUsersDto.PositionId.HasValue)
            {
                getAllQuery = getAllQuery.Where(r => r.PositionId == getAllUsersDto.PositionId);
            }

            if (getAllUsersDto.RoleId.HasValue)
            {
                getAllQuery = getAllQuery.Where(r => r.UserRoles.Any(ur => ur.RoleId == getAllUsersDto.RoleId));
            }

            if (!string.IsNullOrEmpty(getAllUsersDto.PhoneNumber))
            {
                getAllQuery =
                    getAllQuery.Where(r => EF.Functions.ILike(r.PhoneNumber, $"%{getAllUsersDto.PhoneNumber}%"));
            }

            int totalCount = await getAllQuery.CountAsync();

            getAllQuery = OrderBy(getAllUsersDto, getAllQuery);

            getAllQuery = getAllUsersDto.Limit.HasValue
                ? getAllQuery.Skip(getAllUsersDto.Offset).Take(getAllUsersDto.Limit.Value)
                : getAllQuery.Skip(getAllUsersDto.Offset);

            List<User> users = await getAllQuery.ToListAsync();
            PagedResultDto<UserDto> pagedResultDto =
                new PagedResultDto<UserDto>(totalCount, _mapper.Map<List<UserDto>>(users));
            
            return pagedResultDto;
        }

        public async Task CreateAsync(CreateUserDto createUserDto)
        {
            string passwordHash = _passwordHelper.Encrypt(createUserDto.Password);

            var user = _mapper.Map<User>(createUserDto);
            user.PasswordHash = passwordHash;
            
            await _dataContext.Users.AddAsync(user);
            await _dataContext.SaveChangesAsync();            
        }

        public async Task<bool> CheckUsernameUniquenessAsync(CheckUsernameUniquenessDto checkUsernameUniquenessDto)
        {
            bool isExist = await _dataContext.Users.AnyAsync(u => u.UserName == checkUsernameUniquenessDto.Username &&
                                                                 (!checkUsernameUniquenessDto.UserId.HasValue || u.Id != checkUsernameUniquenessDto.UserId));
            return !isExist;
        }

        private static IQueryable<User> OrderBy(GetAllUsersDto getAllUsersDto, IQueryable<User> getAllQuery)
        {
            if (string.IsNullOrEmpty(getAllUsersDto.SortedBy))
            {
                getAllUsersDto.SortedBy = "Id";
                getAllUsersDto.SortedDesc = true;
            }
            else
            {
                getAllUsersDto.SortedBy = getAllUsersDto.SortedBy;
            }


            if (!getAllUsersDto.SortedBy.Equals("FullName") && !getAllUsersDto.SortedBy.Equals("Position"))
            {
                getAllQuery = getAllUsersDto.SortedDesc
                    ? getAllQuery.OrderByDescending(u => EF.Property<object>(u, getAllUsersDto.SortedBy))
                    : getAllQuery.OrderBy(u => EF.Property<object>(u, getAllUsersDto.SortedBy));
            }
            else if (getAllUsersDto.SortedBy.Equals("FullName"))
            {
                getAllQuery = getAllUsersDto.SortedDesc
                    ? getAllQuery.OrderByDescending(u => u.FirstName).ThenByDescending(u => u.LastName)
                        .ThenByDescending(u => u.MiddleName)
                    : getAllQuery.OrderBy(u => u.FirstName).ThenBy(u => u.LastName).ThenBy(u => u.MiddleName);
            }

            return getAllQuery;
        }

        public async Task<IEnumerable<SelectItemDto>> GetPositionsAsync()
        {
            return await _dataContext.Positions
                .AsNoTracking()
                .Select(e => new SelectItemDto()
                {
                    Id = e.Id,
                    Value = e.Name
                }).OrderBy(e => e.Value).ToListAsync();
        }

        public async Task UpdateAsync(int id, UpdateUserDto updateUserDto)
        {
            User user = await _dataContext.Users.Include(x => x.UserRoles).FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"User with id = {id} wasn't found."
                };
            }

            _mapper.Map(updateUserDto, user);
            UpdateUserRoles(user, updateUserDto.RoleIds);

            _dataContext.Users.Update(user);
            await _dataContext.SaveChangesAsync();
        }

        public async Task ChangeStatusAsync(int id)
        {
            User user = await _dataContext.Users.FirstOrDefaultAsync(x => x.Id == id && !x.IsSuperAdmin);

            if (user == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"User with id = {id} wasn't found."
                };
            }

            user.IsActive = !user.IsActive;

            _dataContext.Users.Update(user);
            await _dataContext.SaveChangesAsync();
        }

        public async Task ChangePasswordAsync(int id, ChangeUserPasswordDto changeUserPasswordDto)
        {
            User user = await _dataContext.Users.FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"User with id = {id} wasn't found."
                };
            }

            string passwordHash = _passwordHelper.Encrypt(changeUserPasswordDto.Password);
            user.PasswordHash = passwordHash;            

            _dataContext.Users.Update(user);
            await _dataContext.SaveChangesAsync();
        }

        public async Task ResetPasswordAsync(ResetPasswordDto resetPasswordDto)
        {
            int userId = AuthHelper.GetCurrentUserId(_httpContextAccessor.HttpContext);
            User user = await _dataContext.Users.FirstOrDefaultAsync(x => x.Id == userId);

            if (user == null)
            {                
                throw new HttpResponseException() { Code = "NOT_FOUND_EXCEPTION", StatusCode = 404, Message = "User wasn't found." };
            }

            string oldPassowrdHash = _passwordHelper.Encrypt(resetPasswordDto.OldPassword);

            if (oldPassowrdHash != user.PasswordHash)
            {
                throw new HttpResponseException() { Code = "OLD_PASSWORD_WRONG", StatusCode = 400, Message = "Old Password is incorrect!" };
            }

            if (resetPasswordDto.Password != resetPasswordDto.PasswordRepeat)
            {
                throw new HttpResponseException() { Code = "REPEAT_PASSWORD_WRONG", StatusCode = 400, Message = "Passwords are not match!" };
            }

            string passwordHash = _passwordHelper.Encrypt(resetPasswordDto.Password);
            user.PasswordHash = passwordHash;
            
            _dataContext.Users.Update(user);
            await _dataContext.SaveChangesAsync();
        }

        private void UpdateUserRoles(User user, IList<int> roleIds)
        {
            foreach (int roleId in roleIds)
            {
                var hasUserRoleId = user.UserRoles.Any(x => x.RoleId == roleId);
                if (!hasUserRoleId)
                {
                    user.UserRoles.Add(new UserRole()
                    {
                        UserId = user.Id,
                        RoleId = roleId
                    });
                }
            }

            foreach (UserRole userRole in user.UserRoles)
            {
                var isRoleIdSelected = roleIds.Any(x => x == userRole.RoleId);
                if (!isRoleIdSelected)
                {
                    _dataContext.UserRoles.Remove(userRole);
                }
            }
        }
    }
}
