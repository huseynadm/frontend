﻿using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Security.Claims;

namespace Azfibernet.Helpers
{
    public static class AuthHelper
    {
        public static int GetCurrentUserId(HttpContext context)
        {
            return int.Parse(context.User.Claims.Where(x => x.Type == ClaimTypes.Sid).FirstOrDefault().Value);
        }        
    }
}