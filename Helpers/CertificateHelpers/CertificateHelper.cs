﻿using System.IO;
using System.Security.Cryptography;
using System.Xml.Serialization;

namespace Azfibernet.Helpers.CertificateHelpers
{
    public class CertificateHelper : ICertificateHelper
    {
        private readonly RSAParameters _publicKey;
        private readonly RSAParameters _privateKey;

        public CertificateHelper()
        {
            RSACryptoServiceProvider cryptoService = new RSACryptoServiceProvider(2048);
            _privateKey = cryptoService.ExportParameters(true);
            _publicKey = cryptoService.ExportParameters(false);
        }

        private string GetPublicKey()
        {
            StringWriter sw = new StringWriter();
            XmlSerializer xs = new XmlSerializer(typeof(RSAParameters));
            xs.Serialize(sw, _publicKey);
            return sw.ToString();
        }

        private string GetPrivateKey()
        {
            StringWriter sw = new StringWriter();
            XmlSerializer xs = new XmlSerializer(typeof(RSAParameters));
            xs.Serialize(sw, _privateKey);
            return sw.ToString();
        }

        public (string, string) GetPublicAndPrivateKey()
        {
            return (GetPublicKey(), GetPrivateKey());
        }
    }
}
