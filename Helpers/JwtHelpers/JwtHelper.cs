﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Azfibernet.Helpers.JwtHelpers
{
    public class JwtHelper : IJwtHelper
    {   
        private readonly IConfiguration configuration;

        public JwtHelper(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public string GenerateAccessToken(int userId)
        {
            IList<Claim> tokenClaims = new List<Claim>
            {
                new Claim(ClaimTypes.Sid, userId.ToString()),
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Authentication:JwtKey"]));

            SigningCredentials credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            DateTime expireDate = DateTime.Now.AddMinutes(10000);

            string tokenIssuer = configuration["Authentication:JwtIssuer"];

            string tokenAudience = configuration["Authentication:JwtAudience"];

            string accessToken = new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(
                tokenIssuer,
                tokenAudience,
                tokenClaims,
                expires: expireDate,
                signingCredentials: credentials
                ));

            return accessToken;
        }

        public string GenerateRefreshToken()
        {
            return Guid.NewGuid().ToString().Replace("-", string.Empty);
        }
    }
}
