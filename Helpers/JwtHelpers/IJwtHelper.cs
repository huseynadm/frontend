﻿namespace Azfibernet.Helpers.JwtHelpers
{
    public interface IJwtHelper
    {
        string GenerateAccessToken(int userId);
        string GenerateRefreshToken();
    }
}
