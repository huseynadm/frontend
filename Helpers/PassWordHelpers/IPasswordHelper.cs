﻿namespace Azfibernet.Helpers.PassWordHelpers
{
    public interface IPasswordHelper
    {
        string Encrypt(string password);
    }
}
