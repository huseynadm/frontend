﻿using Azfibernet.Helpers.PassWordHelpers;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Azfibernet.Helpers
{
    public class PasswordHelper : IPasswordHelper
    {
        private readonly IConfiguration _configuration;

        public PasswordHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string Encrypt(string password)
        {
            string encryptionSalt = _configuration["PasswordEncryptionSalt"].ToString();

            return Convert.ToBase64String(KeyDerivation.Pbkdf2(password: password,
                salt: Encoding.UTF8.GetBytes(encryptionSalt),
                prf: KeyDerivationPrf.HMACSHA512,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
        }
    }
}
