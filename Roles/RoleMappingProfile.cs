using Azfibernet.Roles.Dtos;
using Azfibernet.Roles.Models;
using AutoMapper;

namespace Azfibernet.Roles
{
    public class RoleMappingProfile : Profile
    {
        public RoleMappingProfile()
        {
            CreateMap<RoleCreateDto, Role>();                

            CreateMap<UpdateRoleDto, Role>();

            CreateMap<Role, RoleDetailDto>();

            CreateMap<Role, RoleDto>();
        }
    }
}