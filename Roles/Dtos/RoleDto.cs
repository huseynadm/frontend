﻿namespace Azfibernet.Roles.Dtos
{
    public class RoleDto
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public bool IsStatic { get; set; }
        public string Description { get; set; }
    }
}
