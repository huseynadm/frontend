using System.Text.Json.Serialization;

namespace Azfibernet.Roles.Dtos
{
    public class UpdateRoleDto
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string RoleName { get; set; }
        public string RoleKey { get; set; }
        public bool IsStatic { get; set; }
        public string Description { get; set; }
    }
}