using System.Collections.Generic;

namespace Azfibernet.Roles.Dtos
{
    public class RoleCreateDto
    {
        public string RoleName { get; set; }
        public string RoleKey { get; set; }
        public bool IsStatic { get; set; }
        public string Description { get; set; }
    }
}