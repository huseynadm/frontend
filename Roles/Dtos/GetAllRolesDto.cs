﻿using Azfibernet.Shared.Search.Implements;

namespace Azfibernet.Roles.Dtos
{
    public class GetAllRolesDto: PagedAndSortedResultRequestDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
