﻿using Azfibernet.Shared;
using Azfibernet.Users.Models;
using System.Collections.Generic;

namespace Azfibernet.Roles.Models
{
    public class Role : BaseEntity
    {       
        public string RoleName { get; set; }
        public string RoleKey { get; set; }
        public bool IsStatic { get; set; }       
        public string Description { get; set; }
        public IList<UserRole> UserRoles { get; set; }
    }
}
