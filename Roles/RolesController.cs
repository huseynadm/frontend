using System.Threading.Tasks;
using Azfibernet.Common.ActionFilters;
using Azfibernet.Roles.Dtos;
using Azfibernet.Shared;
using Microsoft.AspNetCore.Mvc;

namespace Azfibernet.Roles
{
    public class RolesController : BaseController
    {
        private readonly IRoleService _roleService;

        public RolesController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [HttpGet("roles")]
        [CustomAuthorize("Role:list")]
        public async Task<IActionResult> GetAllAsync([FromQuery] GetAllRolesDto getAllRolesDto)
        {
            return Ok(await _roleService.GetAllAsync(getAllRolesDto));
        }

        [HttpGet("roles/all")]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _roleService.GetAllAsync());
        }

        [HttpGet("roles/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> GetDetailsAsync(int id)
        {
            return Ok(await _roleService.GetDetailsAsync(id));
        }

        [HttpGet("roles/role-key/{roleKey}/is-available")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> IsRoleKeyAvailableAsync(string roleKey)
        {
            return Ok(await _roleService.IsRoleKeyAvailableAsync(roleKey));
        }

        [HttpPost("roles")]
        [CustomAuthorize("Role:add")]
        public async Task<IActionResult> CreateAsync([FromBody] RoleCreateDto roleCreateDto)
        {
            await _roleService.CreateAsync(roleCreateDto);
            return Ok();
        }

        [HttpPut("roles/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> UpdateAsync(int id, [FromBody] UpdateRoleDto updateRoleDto)
        {
            updateRoleDto.Id = id;
            await _roleService.UpdateAsync(updateRoleDto);
            return Ok();
        }

        [HttpDelete("roles/{id}")]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await _roleService.DeleteAsync(id);
            return Ok();
        }
    }
}