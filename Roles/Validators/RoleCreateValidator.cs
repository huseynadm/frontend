using Azfibernet.Roles.Dtos;
using FluentValidation;
using System.Threading.Tasks;

namespace Azfibernet.Roles.Validators
{
    public class RoleCreateValidator : AbstractValidator<RoleCreateDto>
    {
        private readonly IRoleService _roleService;

        public RoleCreateValidator(IRoleService roleService)
        {
            _roleService = roleService;

            RuleFor(e => e.RoleName).NotNull().NotEmpty().WithMessage("Role name is required!");

            RuleFor(e => e.RoleKey).Must((dto, RoleKey) => IsRoleKeyAvailableAsync(RoleKey).Result).WithMessage("Role key is not available!")
                .NotNull().NotEmpty().When(e => e.IsStatic).WithMessage("Role key is required!");            
        }

        private async Task<bool> IsRoleKeyAvailableAsync(string roleKey)
        {
            return await _roleService.IsRoleKeyAvailableAsync(roleKey);
        }
    }
}