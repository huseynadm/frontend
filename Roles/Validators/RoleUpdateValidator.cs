using Azfibernet.Roles.Dtos;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using System;
using System.Threading.Tasks;

namespace Azfibernet.Roles.Validators
{
    public class RoleUpdateValidator : AbstractValidator<UpdateRoleDto>
    {
        private readonly IRoleService _roleService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RoleUpdateValidator(IRoleService roleService, IHttpContextAccessor httpContextAccessor)
        {
            _roleService = roleService;
            _httpContextAccessor = httpContextAccessor;

            RuleFor(e => e.RoleName).NotNull().NotEmpty().WithMessage("Role name is required!");

            RuleFor(e => e.RoleKey).Must((dto, RoleKey) => IsRoleKeyAvailableAsync(RoleKey).Result).WithMessage("Role key is not available!")
                .NotNull().NotEmpty().When(e => e.IsStatic).WithMessage("Role key is required!");
        }

        private async Task<bool> IsRoleKeyAvailableAsync(string roleKey)
        {
            var id = _httpContextAccessor.HttpContext.GetRouteData().Values["id"];
            return await _roleService.IsRoleKeyAvailableAsync(roleKey, Convert.ToInt32(id));
        }
    }
}