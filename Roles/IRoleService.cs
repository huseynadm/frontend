using System.Collections.Generic;
using System.Threading.Tasks;
using Azfibernet.Roles.Dtos;
using Azfibernet.Shared;

namespace Azfibernet.Roles
{
    public interface IRoleService
    {
        Task<PagedResultDto<RoleDto>> GetAllAsync(GetAllRolesDto getAllRolesDto);
        Task<IEnumerable<SelectItemDto>> GetAllAsync();
        Task<RoleDetailDto> GetDetailsAsync(int id);
        Task<bool> IsRoleKeyAvailableAsync(string roleKey, int? id = null);
        Task CreateAsync(RoleCreateDto roleCreateDto);
        Task UpdateAsync(UpdateRoleDto updateRoleDto);
        Task DeleteAsync(int id);
    }
}