using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Azfibernet.ApplicationDataContext;
using Azfibernet.Middlewares.ExceptionMiddlewares;
using Azfibernet.Roles.Dtos;
using Azfibernet.Roles.Models;
using Azfibernet.Shared;
using Microsoft.EntityFrameworkCore;

namespace Azfibernet.Roles
{
    public class RoleService : IRoleService
    {
        private readonly IMapper _mapper;
        private readonly DataContext _dataContext;

        public RoleService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<PagedResultDto<RoleDto>> GetAllAsync(GetAllRolesDto getAllRolesDto)
        {
            IQueryable<Role> getAllQuery = _dataContext.Roles.AsQueryable();
            if (!string.IsNullOrEmpty(getAllRolesDto.Name))
            {
                getAllQuery = getAllQuery.Where(r => EF.Functions.ILike(r.RoleName, $"%{getAllRolesDto.Name}%"));
            }

            if (!string.IsNullOrEmpty(getAllRolesDto.Description))
            {
                getAllQuery =
                    getAllQuery.Where(r => EF.Functions.ILike(r.Description, $"%{getAllRolesDto.Description}%"));
            }

            int totalCount = await getAllQuery.CountAsync();

            string sortedBy = !string.IsNullOrEmpty(getAllRolesDto.SortedBy)
                ? getAllRolesDto.SortedBy
                : "CreatedDate";
            getAllQuery = getAllRolesDto.SortedDesc
                ? getAllQuery.OrderByDescending(r => EF.Property<object>(r, sortedBy))
                : getAllQuery.OrderBy(r => EF.Property<object>(r, sortedBy));

            getAllQuery = getAllRolesDto.Limit.HasValue
                ? getAllQuery.Skip(getAllRolesDto.Offset).Take(getAllRolesDto.Limit.Value)
                : getAllQuery.Skip(getAllRolesDto.Offset);

            List<Role> roles = await getAllQuery.ToListAsync();
            PagedResultDto<RoleDto> pagedResultDto =
                new PagedResultDto<RoleDto>(totalCount, _mapper.Map<List<RoleDto>>(roles));

            return pagedResultDto;
        }

        public async Task<IEnumerable<SelectItemDto>> GetAllAsync()
        {
            return await _dataContext.Roles
                .AsNoTracking()
                .Select(e => new SelectItemDto()
                {
                    Id = e.Id,
                    Value = e.RoleName
                }).ToListAsync();
        }

        public async Task<RoleDetailDto> GetDetailsAsync(int id)
        {
            Role role = await _dataContext.Roles.FirstOrDefaultAsync(e => e.Id == id);

            if (role == null)
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = "Role wasn't found."
                };

            return _mapper.Map<RoleDetailDto>(role);
        }

        public async Task<bool> IsRoleKeyAvailableAsync(string roleKey, int? id = null)
        {
            bool isAvailable = await _dataContext.Roles.AllAsync(e => e.RoleKey.ToUpper() != roleKey.ToUpper() || (id.HasValue && e.Id == id));
            return isAvailable;
        }

        public async Task CreateAsync(RoleCreateDto roleCreateDto)
        {
            await _dataContext.Roles.AddAsync(_mapper.Map<Role>(roleCreateDto));
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(UpdateRoleDto updateRoleDto)
        {
            Role updateRole = _mapper.Map<Role>(updateRoleDto);
            Role role = await _dataContext.Roles
                .FirstOrDefaultAsync(x => x.Id == updateRole.Id);

            if (role == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = "Role wasn't found."
                };
            }

            if (role.IsStatic && (updateRoleDto.RoleKey != role.RoleKey || !updateRoleDto.IsStatic))
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_ACCESS_EXCEPTION",
                    StatusCode = 403,
                    Message = "Role key, is static could not be changed for static roles."
                };
            }

            _mapper.Map(updateRoleDto, role);

            _dataContext.Roles.Update(role);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var role = await _dataContext.Roles.AsNoTracking()
                .Include(e => e.UserRoles)
                .FirstOrDefaultAsync(e => e.Id == id);

            if (role == null)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_FOUND_EXCEPTION",
                    StatusCode = 404,
                    Message = $"No role with id = {id} was found"
                };
            }

            if (role.IsStatic)
            {
                throw new HttpResponseException()
                {
                    Code = "NOT_ACCESS_EXCEPTION",
                    StatusCode = 403,
                    Message = "Static roles cannot be deleted."
                };
            }

            _dataContext.Roles.Remove(role);
            await _dataContext.SaveChangesAsync();            
        }
    }
}